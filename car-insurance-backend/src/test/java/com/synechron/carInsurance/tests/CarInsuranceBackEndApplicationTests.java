package com.synechron.carInsurance.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.synechron.carInsurance.Repository.CarRepository;
import com.synechron.carInsurance.Service.CarService;
import com.synechron.carInsurance.json.CarDTO;
import com.synechron.carInsurance.model.car.Car;

@SpringBootTest
class CarInsuranceBackEndApplicationTests {

	@Mock(answer=Answers.RETURNS_DEEP_STUBS)
	CarRepository carRepository;

	@Autowired
	CarService cs;

	@Test
	void updateCarTest() {
		Long l = new Long(2);
		Car c = new Car(l, "Renault", "Clio", 2016, false);
		Optional<Car> oc = Optional.of(c);
		when(carRepository.findById(l)).thenReturn(oc);		
		c = cs.updateCar(l, new CarDTO("Renault", "Clio", "2016", null, null));
		
		assertEquals(c.getBrand(), "Renault");
		assertEquals(c.getModel(), "Clio");
		assertEquals(c.getYear(), 2016);
				
	}

	@Test
	void searchCarsTest() {
		CarDTO cd1 = new CarDTO("Renault", "Clio", "2016", null, null);
		CarDTO cd3 = new CarDTO("Renault", "", "", null, null);
		CarDTO cd4 = new CarDTO("", "Clio", "", null, null);
		CarDTO cd5 = new CarDTO("", "", "2016", null, null);
		CarDTO cd2 = new CarDTO("Renault", "Clio", "", null, null);
		CarDTO cd6 = new CarDTO("Renault", "", "2016", null, null);
		CarDTO cd7 = new CarDTO("", "Clio", "2016", null, null);
		CarDTO cd0 = new CarDTO("", "", "", null, null);
		Pageable pageable = PageRequest.of(0, 5);
		
		when(carRepository.findByBrandContaining("Renault", pageable).size()).thenReturn(3);
		when(carRepository.findByModelContaining("Clio", pageable).size()).thenReturn(2);
		when(carRepository.findByYear(2016, pageable).size()).thenReturn(1);
		when(carRepository.findByBrandAndModelContaining("Renault", "Clio", pageable).size()).thenReturn(2);
		when(carRepository.findByBrandAndYearContaining("Renault", 2016, pageable).size()).thenReturn(1);
		when(carRepository.findByModelAndYearContaining("Clio", 2016, pageable).size()).thenReturn(1);
		when(carRepository.findAllCarsWithBrandAndModelAndYearPageable("Renault", "Clio", 2016, pageable).size()).thenReturn(1);

		assertEquals(cs.searchCars(cd1, 0, 5).size(), 1);
		assertEquals(cs.searchCars(cd3, 0, 5).size(), 3);
		assertEquals(cs.searchCars(cd4, 0, 5).size(), 2);
		assertEquals(cs.searchCars(cd5, 0, 5).size(), 1);
		assertEquals(cs.searchCars(cd2, 0, 5).size(), 2);
		assertEquals(cs.searchCars(cd6, 0, 5).size(), 1);
		assertEquals(cs.searchCars(cd7, 0, 5).size(), 1);
		assertEquals(cs.searchCars(cd0, 0, 5).size(), 0);

	}
}
