
INSERT INTO car(brand, model, year) VALUES ('Renault', 'Clio', 2015);
INSERT INTO car(brand, model, year) VALUES ('Renault', 'Clio', 2016);
INSERT INTO car(brand, model, year) VALUES ('Renault', 'Talisman', 2018);
INSERT INTO car(brand, model, year) VALUES ('Citroen', 'C4', 2021);
INSERT INTO car(brand, model, year) VALUES ('Kia', 'Sportage', 2012);



--address
INSERT INTO country(name)  VALUES ('Srbija');
INSERT INTO country(name)  VALUES ('Bosna i Hercegovina');
INSERT INTO country(name)  VALUES ('Makedonija');
INSERT INTO country(name)  VALUES ('Hrvatska');
INSERT INTO country(name)  VALUES ('Crna Gora');

INSERT INTO city(name,zipcode,country_id) VALUES ('Novi Sad', '21000','1');
INSERT INTO city(name,zipcode,country_id) VALUES ('Beograd', '11000','1');
INSERT INTO Proposal (amount)
VALUES (1);
INSERT INTO address(street, city_id) VALUES ('Vuka Karadzica','1');
INSERT INTO address(street, city_id) VALUES ('Misarska','2');

--subscriber

INSERT INTO gender(name) VALUES ('female');
INSERT INTO gender(name) VALUES ('male');
INSERT INTO gender(name) VALUES ('other');
INSERT INTO marital_status(name) VALUES ('single');
INSERT INTO marital_status(name) VALUES ('married');
INSERT INTO marital_status(name) VALUES ('divorced');
INSERT INTO marital_status(name) VALUES ('widowed');
INSERT INTO marital_status(name) VALUES ('other');

INSERT INTO subscriber_role(name) VALUES ('prospekt');
INSERT INTO subscriber_role(name) VALUES ('klijent');

INSERT INTO subscriber(birth,email,home_phone,mobile_phone,first_name, gender_id, jmbg, last_name, marital_status_id, address_id, subscriber_role_id) VALUES
('2017-10-11','sneki@gmail.com', '021458458415', '0645478545', 'snezana', '1', '565965656', 'Bulajic', '2', '1','1');


--risks
INSERT INTO risk(description) VALUES ('kockanje');
INSERT INTO risk(description) VALUES ('istorija');
INSERT INTO risk(description) VALUES ('alkohol');

--insurance plan, insurance items

INSERT INTO insurance_item(name,is_optional, amount) VALUES ('i1',false, '100');
INSERT INTO insurance_item(name,is_optional, amount) VALUES ('i2',false, '200');
INSERT INTO insurance_item(name,is_optional, amount) VALUES ('i3',false, '300');
INSERT INTO insurance_item(name,is_optional, amount) VALUES ('i4',true, '400');

INSERT INTO insurance_plan(name, is_premium,amount) VALUES ('basic', false,'300');
INSERT INTO insurance_plan(name, is_premium,amount) VALUES ('normal', false,'0');
INSERT INTO insurance_plan(name, is_premium,amount) VALUES ('premium', true,'300');

INSERT INTO insurance_plan_insurance_items(insurance_plan_id, insurance_items_id) VALUES ('1','1');
INSERT INTO insurance_plan_insurance_items(insurance_plan_id, insurance_items_id) VALUES ('1','2');

INSERT INTO insurance_plan_insurance_items(insurance_plan_id, insurance_items_id) VALUES ('3','1');
INSERT INTO insurance_plan_insurance_items(insurance_plan_id, insurance_items_id) VALUES ('3','2');

--payment
INSERT INTO payment_mode(name) VALUES ('Credit Card');
INSERT INTO payment_mode(name) VALUES ('Cheque Payment');
INSERT INTO payment_mode(name) VALUES ('Bank Payment');
INSERT INTO payment_mode(name) VALUES ('Cash');

INSERT INTO credit_card_type(name) VALUES ('Master Card');
INSERT INTO credit_card_type(name) VALUES ('Visa');
INSERT INTO credit_card_type(name) VALUES ('Dina');
INSERT INTO credit_card_type(name) VALUES ('American Express');

--users
--INSERT INTO user_role(name) VALUES ('Admin');
--INSERT INTO user_role(name) VALUES ('Agent');

INSERT INTO user(username, password, user_role) VALUES ('admin', 'admin','0');
INSERT INTO user(username, password,user_role) VALUES ('agent', 'agent','1');

