package listener;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.synechron.carInsurance.CarInsuranceBackEndApplication;
import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.Service.EmailService;
import com.synechron.carInsurance.model.proposal.Proposal;

@Component
public class MessageListener {
	
	@Autowired
	ProposalRepository proposalRepository;
	EmailService emailService;

	  @JmsListener(destination = CarInsuranceBackEndApplication.RECEIPT_MESSAGE, containerFactory = "jmsFactory")
	    public void receiveMessage(Map<String, String> message) {
	        System.out.println("Received <" + message + ">");
	        
	        System.out.println("Message processed...");
	        emailService.sendEmail(message);
	    }
}
