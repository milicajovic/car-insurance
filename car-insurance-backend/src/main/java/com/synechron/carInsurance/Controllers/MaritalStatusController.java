package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.CityService;
import com.synechron.carInsurance.Service.MaritalStatusService;
import com.synechron.carInsurance.model.users.City;
import com.synechron.carInsurance.model.users.MaritalStatus;

@RestController
@RequestMapping(path = "api/v1/maritalStatuses")
public class MaritalStatusController {
	
private MaritalStatusService maritalStatusService;
	
	@Autowired
	public MaritalStatusController(MaritalStatusService maritalStatusService) {
		this.maritalStatusService = maritalStatusService;
	}
	
	@GetMapping()
	public List<MaritalStatus> getCountries(){
		return maritalStatusService.getMaritalStatuses();
		
	}
}
