package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.CarService;
import com.synechron.carInsurance.Service.CityService;
import com.synechron.carInsurance.Service.CountryService;
import com.synechron.carInsurance.model.users.City;
import com.synechron.carInsurance.model.users.Country;

@RestController
@RequestMapping(path = "api/v1/city")
public class CityController {

	

	private CityService cityService;
	
	@Autowired
	public CityController(CityService cityService) {
		this.cityService = cityService;
	}
	
	@GetMapping("/{countryId}")
	public List<City> getCountries(@PathVariable Long countryId){
		return cityService.getCitiesOfCountry(countryId);
		
	}
}