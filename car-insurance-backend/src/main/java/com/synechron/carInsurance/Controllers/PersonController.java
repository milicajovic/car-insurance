package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.PersonService;
import com.synechron.carInsurance.json.PersonDTO;
import com.synechron.carInsurance.json.PersonDTO2;

@RestController
@RequestMapping(path = "api/v1/person")
public class PersonController {

	private final PersonService personService;
	
	@Autowired
	public PersonController(PersonService personService) {
		this.personService = personService;
	}
	
	@GetMapping(path = "{page}/{pageSize}")
	public List<PersonDTO> getAll(@PathVariable("page") int page, @PathVariable("pageSize") int pageSize){
		return this.personService.getAll(page, pageSize);
	}
	
	@PutMapping(path = "editPerson")
	public void editPerson(@RequestBody PersonDTO person) {
		this.personService.editPerson(person);
	}
	
	@DeleteMapping(path = "{jmbg}")
	public void deletePerson(@PathVariable String jmbg) {
		this.personService.deletePerson(jmbg);
	}
	
	@PostMapping(path = "searchPeople")
	public List<PersonDTO> searchPeople(@RequestBody PersonDTO2 person, @PathVariable("page") int page, @PathVariable("pageSize") int pageSize) {
		return this.personService.searchPeople(person.getFirstName(), person.getLastName(), person.getJmbg(), page, pageSize);
	}
}
