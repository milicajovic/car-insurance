package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.AddressService;
import com.synechron.carInsurance.Service.GenderService;
import com.synechron.carInsurance.Service.MaritalStatusService;
import com.synechron.carInsurance.Service.ProposalService;
import com.synechron.carInsurance.Service.SubscriberRoleService;
import com.synechron.carInsurance.Service.SubscriberService;
import com.synechron.carInsurance.json.SubscriberDTO;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Contact;
import com.synechron.carInsurance.model.users.Subscriber;

@RestController
@RequestMapping(path = "api/v1/subscriber")
public class SubscriberController {

	private final SubscriberService subscriberService;
	private final MaritalStatusService maritalStatusService;
	private final GenderService genderService;
	private final SubscriberRoleService subscriberRoleService;
	private final AddressService addressService;
	private final ProposalService proposalService;
	
	
	@Autowired
	public SubscriberController(SubscriberService subscriberService, MaritalStatusService maritalStatusService, 
			GenderService genderService, SubscriberRoleService subscriberRoleService, AddressService addressService, 
			ProposalService proposalService) {
		
		this.subscriberService = subscriberService;
		this.addressService = addressService;
		this.genderService = genderService;
		this.maritalStatusService = maritalStatusService;
		this.subscriberRoleService = subscriberRoleService;
		this.proposalService = proposalService;
	}
	
	@GetMapping
	public List<Subscriber> getSubscribers(){
		return subscriberService.getSubscribers();
	}
	
	@PostMapping
	public Proposal addSubscriber(@RequestBody SubscriberDTO subscriber) {
		Subscriber sub = new Subscriber();
		sub.firstName = subscriber.getFirstName();
		sub.setLastName(subscriber.getLastName());
		sub.setJmbg(subscriber.getJmbg());
		sub.setBirth(subscriber.getBirth());
		Contact c = new Contact(subscriber.getHomePhone(), subscriber.getMobilePhone(), subscriber.getEmail());
		sub.setContact(c);
		
		if(subscriber.getGender() != null)
			sub.setGender(genderService.findGenderById(subscriber.getGender()));
		
		if(subscriber.getMaritalStatus() != null)
			sub.setMaritalStatus(maritalStatusService.findMaritalStatusById(subscriber.getMaritalStatus()));
		
		if(subscriber.getSubscriberRole() != null)
			sub.setSubscriberRole(subscriberRoleService.findSubscriberRoleById(subscriber.getSubscriberRole()));
		
		if(subscriber.getAddress() != null)
			sub.setAddress(addressService.findAddressByID(subscriber.getAddress()));
		
		subscriberService.addSubscriber(sub);
		
		Proposal proposal = new Proposal();
		proposal.setSubscriber(sub);
		proposal.setIsValid(false);
		this.proposalService.addNewProposal(proposal);
		return proposal;
	}
	
	@GetMapping(path = "{jmbg}")
	public Subscriber findSubscriber(@PathVariable("jmbg") String jmbg) {
		return subscriberService.findSubscriberByJmbg(jmbg);
	}
}
