package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.InsurancePlanService;
import com.synechron.carInsurance.json.AddingItemDTO;
import com.synechron.carInsurance.json.SettingPlanOnProposal;
import com.synechron.carInsurance.model.proposal.InsuranceItem;
import com.synechron.carInsurance.model.proposal.InsurancePlan;
import com.synechron.carInsurance.model.proposal.Proposal;



@RestController
@RequestMapping(path = "/api/v1/insurancePlan")
public class InsurancePlanController {
	
	private final InsurancePlanService insurancePlanService;
	
	
	@Autowired
	public InsurancePlanController(InsurancePlanService insurancePlanService) {
		this.insurancePlanService = insurancePlanService;
	}
	
	@GetMapping("allInsurancePlans")
	public List<InsurancePlan> getInsurancePlans(){
		return insurancePlanService.getInsurancePlans();
	}
	
	@GetMapping("ItemsOfInsurancePlan/{id}")
	public List<InsuranceItem> getInsuranceItems(@PathVariable("id") Long id){
		return insurancePlanService.getInsuranceItems(id);
	}
	@GetMapping("AllInsuranceItems")
	public List<InsuranceItem> getAllInsuranceItems(){
		return insurancePlanService.getAllInsuranceItems();
	}
	@PostMapping("addItem")
	public InsurancePlan addInsuranceItem(@RequestBody AddingItemDTO addingItem) {
		System.out.println(addingItem.getItemId()+"kontroller"+addingItem.getPlanId());
		return insurancePlanService.addInsuranceItem(addingItem.getItemId(),addingItem.getPlanId());
	}
	@PostMapping("setPlanOnProposal")
	public Proposal setPlanOnProposal(@RequestBody SettingPlanOnProposal settingPlanOnProposal) {
		return insurancePlanService.setPlanOnProposal(settingPlanOnProposal.getPlan(),settingPlanOnProposal.getIdProposal());
	}
	@GetMapping("priceOfInsurancePlan/{id}")
	public Double getPriceOfInsurancePlan(@PathVariable("id") Long id){
		return insurancePlanService.getPriceOfInsurancePlan(id);
	}
}
