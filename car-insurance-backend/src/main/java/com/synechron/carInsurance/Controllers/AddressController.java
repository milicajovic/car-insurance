package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.AddressService;
import com.synechron.carInsurance.model.users.Address;
import com.synechron.carInsurance.model.users.City;

@RestController
@RequestMapping(path = "api/v1/address")
public class AddressController {
	
	private AddressService addressService;
	
	@Autowired
	public AddressController(AddressService addressService) {
		this.addressService = addressService;
	}
	
	@GetMapping("/{cityId}")
	public List<Address> getAddresses( @PathVariable Long cityId ){
		return addressService.getAddressesOfCities(cityId);
		
	}
}
