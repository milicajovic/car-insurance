package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.synechron.carInsurance.Service.FranchiseService;
import com.synechron.carInsurance.Service.InsuranceItemService;
import com.synechron.carInsurance.Service.ProposalService;
import com.synechron.carInsurance.json.FranchiseDTO;
import com.synechron.carInsurance.json.ProposalSubscriberDTO;
import com.synechron.carInsurance.model.proposal.Franchise;
import com.synechron.carInsurance.model.proposal.InsuranceItem;
import com.synechron.carInsurance.model.proposal.Proposal;



@RestController
@RequestMapping(path = "/api/v1/franchiseController")
public class FranchiseController {
	private final ProposalService proposalService;
	private final InsuranceItemService insuranceItemService;
	private final FranchiseService franchiseService;
	
	@Autowired
	public FranchiseController(FranchiseService franchiseService,ProposalService proposalService,InsuranceItemService insuranceItemService) {
		this.proposalService = proposalService;
		this.franchiseService = franchiseService;
		this.insuranceItemService = insuranceItemService;
	}
	@GetMapping()
	public List<Franchise> getFranchises(){
		return franchiseService.getFranchises();
	}
	@PostMapping()
	public ResponseEntity<Franchise> addFranchise(@RequestBody FranchiseDTO franchise) {
		Proposal p = proposalService.getProposal(franchise.getProposalID()).get();
		InsuranceItem ii = insuranceItemService.getItemById(franchise.getInsuranceItemID());
		Franchise f = franchiseService.addFranchise(franchise.getPercentage(),p,ii);
		return new ResponseEntity<>(f, HttpStatus.OK);
	}
}
