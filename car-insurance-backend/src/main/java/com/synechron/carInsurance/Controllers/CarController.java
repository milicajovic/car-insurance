package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.CarService;
import com.synechron.carInsurance.Service.ProposalService;

import com.synechron.carInsurance.json.CarDTO;
import com.synechron.carInsurance.json.SettingLicencePlateDTO;
import com.synechron.carInsurance.model.car.Brand;
import com.synechron.carInsurance.model.car.Car;
import com.synechron.carInsurance.model.car.Model;
import com.synechron.carInsurance.model.proposal.Proposal;

@RestController
@RequestMapping(path = "api/v1/car")
public class CarController {

	private final CarService carService;
	
	@Autowired
	public CarController(CarService carService) {
		this.carService = carService;
	}
	
	@GetMapping("allCars")
	public List<Car> getCars(){
		return carService.getCars();
	}
	
	@GetMapping("allCars/{page}/{pageSize}")
	public List<Car> getCarsPageable(@PathVariable int page, @PathVariable int pageSize){
		return carService.getCarsPageable(page, pageSize);
	}
	@GetMapping("/withBrand/{brand}")
	public List<Car> getCarsWithBrand(@PathVariable String brand){
		return carService.getCarsWithBrand(brand);
	}
	
	
	@GetMapping("/withBrandAndModel/{brand}/{model}")
	public List<Car> getCarsWithBrandAndModel(@PathVariable String brand,@PathVariable String model){
		return carService.getCarsWithBrandAndModel(brand,model);
	}
	
	@GetMapping("/withBrandModelAndYear/{brand}/{model}/{year}")
	public Car getCarsWithBrandModelAndYear(@PathVariable String brand,@PathVariable String model,@PathVariable Integer year){
		return  carService.getCarWithBrandModelAndYear(brand,model,year);
	}
	
	@PostMapping
	public Car addNewCar(@RequestBody Car car) {
		return carService.addNewCar(car);
	}
	
	@DeleteMapping(path = "{carId}")
	public Car deleteCar(@PathVariable("carId") Long carId) {
		return carService.deleteCar(carId);
	}
	
	@GetMapping("/allBrands")
	public List<String> getBrands(){
		return carService.getBrands();
	}
	
	@GetMapping("/allModels")
	public List<String> getModels(){
		return carService.getModels();
	}
	
	@PostMapping("/searchCars/{page}/{pageSize}")
	public List<Car> searchCars(@RequestBody CarDTO car, @PathVariable("page") int page, @PathVariable("pageSize") int pageSize){
		return carService.searchCars(car, page, pageSize);
	}
	
	@PutMapping("/edit/{carId}")
	public Car editCar(@PathVariable("carId") Long carId, @RequestBody CarDTO carDTO) {
		return carService.updateCar(carId, carDTO);
	}
	
	
//	@PostMapping
//	public void addCarToProposal(@RequestBody CarDTO carDto) {
//
//		Car car = carService.findByBrandAndModelAndYear(carDto.getBrand(), carDto.getModel(), carDto.getYear());
//	
//		Proposal prop = ProposalService.getById(carDto.getProposalId());
//		
//		prop.setCar(car);
//		prop.setLicenceNum(carDto.getLicencePlate());
//		
//		save prop
//	}
}
