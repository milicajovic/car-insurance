package com.synechron.carInsurance.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.UserService;
import com.synechron.carInsurance.model.users.User;

@RestController
@RequestMapping("api/v1/login")
public class LoginController {
	
	private final UserService userService;
	
	@Autowired
	public LoginController(UserService userService) {
		this.userService=userService;
	}
	
	@PostMapping("loginUser")
    public User loginUser(@RequestBody User user) throws Exception {
    	String username=user.getUsername();
    	String pass=user.getPassword();
    	User u=null;
    	if(username != null && pass != null) {
    		u=userService.findByUsernameAndPassword(username, pass);
    	}
    	if(u == null) {
    		throw new Exception(username + " ne postoji");
    	}
    	return u;
    }


}
