package com.synechron.carInsurance.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.AccidentHistoryService;
import com.synechron.carInsurance.Service.AddressService;
import com.synechron.carInsurance.Service.DriverService;
import com.synechron.carInsurance.Service.GenderService;
import com.synechron.carInsurance.Service.MaritalStatusService;
import com.synechron.carInsurance.Service.ProposalService;
import com.synechron.carInsurance.Service.RiskService;
import com.synechron.carInsurance.json.AccidentHistoryDTO;
import com.synechron.carInsurance.json.DriverDTO;

import com.synechron.carInsurance.json.DriversListDTO;
import com.synechron.carInsurance.model.car.AccidentHistory;
import com.synechron.carInsurance.model.car.Risk;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Contact;
import com.synechron.carInsurance.model.users.Driver;


@RestController
@RequestMapping(path = "/api/v1/driver")
public class DriverController {
	
	private final DriverService driverService;
	private final ProposalService proposalService;
	private final MaritalStatusService maritalStatusService;
	private final GenderService genderService;
	private final AddressService addressService;
	private final AccidentHistoryService accidentHistoryService;
	private final RiskService riskService;
	
	@Autowired
	public DriverController(DriverService driverService,ProposalService proposalService, MaritalStatusService maritalStatusService, GenderService genderService, AddressService addressService,AccidentHistoryService accidentHistoryService, RiskService riskService) {
		this.driverService=driverService;
		this.proposalService = proposalService;
		this.maritalStatusService=maritalStatusService;
		this.genderService=genderService;
		this.addressService=addressService;
		this.accidentHistoryService=accidentHistoryService;
		this.riskService=riskService;
	}
	
	@GetMapping("allDrivers")
	public List<Driver> getDrivers(){
		return driverService.getDrivers();
	}
	
	@GetMapping("searchDriver/{jmbg}")
	public Driver findDriver(@PathVariable("jmbg") String jmbg) {
		return driverService.findDriverByJmbg(jmbg);
	}
	
	@PostMapping("addDriver")
	public ResponseEntity<DriverDTO> newDriver(@RequestBody DriverDTO driver){
		Driver drv=new Driver();
		
		drv.setFirstName(driver.getFirstName());
		drv.setLastName(driver.getLastName());
		drv.setBirth(driver.getBirth());
		drv.setJmbg(driver.getJmbg());
		drv.setBirth(driver.getBirth());
		Contact c = new Contact(driver.getHomePhone(), driver.getMobilePhone(), driver.getEmail());
		drv.setContact(c);
		
		if(driver.getGender() != null)
			drv.setGender(genderService.findGenderById(driver.getGender()));
		
		if(driver.getMaritalStatus() != null)
			drv.setMaritalStatus(maritalStatusService.findMaritalStatusById(driver.getMaritalStatus()));
		
		if(driver.getAddress() != null)
			drv.setAddress(addressService.findAddressByID(driver.getAddress()));
		
		drv.setLicenceNum(driver.getLicenceNum());
		drv.setYearsInsured(driver.getYearsInsured());
		drv.setLicenceObtained(driver.getLicenceObtained());
		
		driverService.addDriver(drv);
		
		return new ResponseEntity<>(HttpStatus.OK);
		
	}

	@PostMapping("addDriversOnProposal/{proposalID}")
	public ResponseEntity<Long> addDriversOnProposal(@RequestBody DriversListDTO drivers,@PathVariable("proposalID") String proposalID) {
		List<Driver> l = new ArrayList<Driver>();
		int size = drivers.getDrivers().size();
		for (int i = 0; i < size; i++ ) {
			Driver drv = driverService.findDriverByJmbg(drivers.getDrivers().get(i).getJmbg());
			if ( drv == null ) {
				DriverDTO driver = drivers.getDrivers().get(i);
				drv=new Driver();
				
				drv.setFirstName(driver.getFirstName());
				drv.setLastName(driver.getLastName());
				drv.setBirth(driver.getBirth());
				drv.setJmbg(driver.getJmbg());
				drv.setBirth(driver.getBirth());
				Contact c = new Contact(driver.getHomePhone(), driver.getMobilePhone(), driver.getEmail());
				drv.setContact(c);
				
				if(driver.getGender() != null)
					drv.setGender(genderService.findGenderById(driver.getGender()));
				
				if(driver.getMaritalStatus() != null)
					drv.setMaritalStatus(maritalStatusService.findMaritalStatusById(driver.getMaritalStatus()));
				
				if(driver.getAddress() != null)
					drv.setAddress(addressService.findAddressByID(driver.getAddress()));
				
				drv.setLicenceNum(driver.getLicenceNum());
				drv.setYearsInsured(driver.getYearsInsured());
				drv.setLicenceObtained(driver.getLicenceObtained());
				
				driverService.addDriver(drv);
				l.add(drv);
			} else {
				driverService.addDriver(drv);
				l.add(drv);
			}
		}
		proposalService.addDriversOnProposal(l,Long.parseLong(proposalID));
		Proposal p2 = proposalService.getProposal(Long.parseLong(proposalID)).get();
		System.out.println(p2.getDrivers()+"0000000000000000");
		return new ResponseEntity<>(Long.parseLong(proposalID),HttpStatus.OK);
	}
	
	@PostMapping("accidentHistory/{id}")
	public void addAccidentHistory(@RequestBody AccidentHistoryDTO accidentHistory, @PathVariable("id") Driver id) {
		AccidentHistory ahistory= new AccidentHistory();
		
		ahistory.setDriver(id);
		ahistory.setHappened(accidentHistory.getHappened());
		ahistory.setDescription(accidentHistory.getDescription());
		ahistory.setIsDeleted(false);
		ahistory.setWasResponsible(false);
		
		accidentHistoryService.newAccidentHistory(ahistory);
		
	}
	

	
	@GetMapping("allRisks")
	public List<Risk> getRisks(){
		return riskService.allRisks();
	}
	


}


	
	
	


