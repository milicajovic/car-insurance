package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.GenderService;
import com.synechron.carInsurance.Service.MaritalStatusService;
import com.synechron.carInsurance.model.users.Gender;
import com.synechron.carInsurance.model.users.MaritalStatus;

@RestController
@RequestMapping(path = "api/v1/gender")
public class GenderController {

	private GenderService genderService;
	@Autowired
	public GenderController(GenderService genderService) {
		this.genderService = genderService;
	}
	
	@GetMapping()
	public List<Gender> getGenders(){
		return genderService.getGenders();
		
	}
}
