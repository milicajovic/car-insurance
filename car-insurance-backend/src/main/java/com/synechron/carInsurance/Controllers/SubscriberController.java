package com.synechron.carInsurance.Controllers;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.AddressService;
import com.synechron.carInsurance.Service.GenderService;
import com.synechron.carInsurance.Service.MaritalStatusService;
import com.synechron.carInsurance.Service.ProposalService;
import com.synechron.carInsurance.Service.SubscriberRoleService;
import com.synechron.carInsurance.Service.SubscriberService;
import com.synechron.carInsurance.json.ProposalSubscriberDTO;
import com.synechron.carInsurance.json.SubscriberDTO;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Contact;
import com.synechron.carInsurance.model.users.Subscriber;

@RestController
@RequestMapping(path = "api/v1/subscriber")
public class SubscriberController {

	private final SubscriberService subscriberService;
	private final MaritalStatusService maritalStatusService;
	private final GenderService genderService;
	private final SubscriberRoleService subscriberRoleService;
	private final AddressService addressService;
	private final ProposalService proposalService;

	@Autowired
	public SubscriberController(SubscriberService subscriberService, MaritalStatusService maritalStatusService,
			GenderService genderService, SubscriberRoleService subscriberRoleService, AddressService addressService,
			ProposalService proposalService) {

		this.subscriberService = subscriberService;
		this.addressService = addressService;
		this.genderService = genderService;
		this.maritalStatusService = maritalStatusService;
		this.subscriberRoleService = subscriberRoleService;
		this.proposalService = proposalService;
	}

	@GetMapping("allSubscribers")
	public ResponseEntity<List<Subscriber>> getSubscribers() {
		return new ResponseEntity<>(subscriberService.getSubscribers(), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<ProposalSubscriberDTO> addSubscriber(@RequestBody SubscriberDTO subscriber) {
		Subscriber sub = new Subscriber();
		sub.firstName = subscriber.getFirstName();
		sub.setLastName(subscriber.getLastName());
		sub.setJmbg(subscriber.getJmbg());
		System.out.println("----------"+subscriber.getBirth()+"---------");
		sub.setBirth(subscriber.getBirth());
		Contact c = new Contact(subscriber.getHomePhone(), subscriber.getMobilePhone(), subscriber.getEmail());
		sub.setContact(c);

		if (subscriber.getGender() != null)
			sub.setGender(genderService.findGenderById(subscriber.getGender()));

		if (subscriber.getMaritalStatus() != null)
			sub.setMaritalStatus(maritalStatusService.findMaritalStatusById(subscriber.getMaritalStatus()));

		if (subscriber.getSubscriberRole() != null)
			sub.setSubscriberRole(subscriberRoleService.findSubscriberRoleById(subscriber.getSubscriberRole()));

		if (subscriber.getAddress() != null)
			sub.setAddress(addressService.findAddressByID(subscriber.getAddress()));

		if (subscriberService.subscriberExists(subscriber.getJmbg())) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

		}

		subscriberService.addSubscriber(sub);

		Proposal proposal = new Proposal();
		proposal.setSubscriber(sub);
		proposal.setIsValid(false);
		this.proposalService.addNewProposal(proposal);

		ProposalSubscriberDTO psd = new ProposalSubscriberDTO(proposal.getId(), sub.getId());
		return new ResponseEntity<>(psd, HttpStatus.OK);

	}

	// ovo celo treba menjati, ne valja logika.... ja kao korisnik moram da mogu i
	// da samo searchujem, a ne da mi se kreira odmah novi proposal, krenuo sam to
	// da prepravljam
/*	@GetMapping(path = "search/{jmbg}")
	@ResponseBody
	public ResponseEntity<ProposalSubscriberDTO> //]findSubscriber(@PathVariable("jmbg") String jmbgS) {

		String jmbg = jmbgS.replace("\"", "");
		System.out.println(jmbg);
		if (!subscriberService.subscriberExists(jmbg)) {

			return ResponseEntity.badRequest().body(null);
		}
		Subscriber s = subscriberService.findSubscriberByJmbg(jmbg);
		return ResponseEntity.ok().body(new ProposalSubscriberDTO(null, s.getId()));
	}*/

	@GetMapping("search/{jmbg}")
	public Subscriber findSubscriber(@PathVariable("jmbg") String jmbg) {
		return subscriberService.findSubscriberByJmbg(jmbg);

	}
	@GetMapping("searchById/{id}")
	public Subscriber findSubscriberById(@PathVariable("id") String id) {
		return subscriberService.findSubscriberById(Long.parseLong(id));

	}
}
