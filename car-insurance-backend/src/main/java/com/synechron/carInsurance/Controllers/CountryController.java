package com.synechron.carInsurance.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Service.CountryService;
import com.synechron.carInsurance.model.users.Country;



@RestController
@RequestMapping(path = "api/v1/country")
public class CountryController {
	
	@Autowired
	CountryService countryService;
	@GetMapping
	public List<Country> getCountries(){
		return countryService.getCountries();
	}
}
