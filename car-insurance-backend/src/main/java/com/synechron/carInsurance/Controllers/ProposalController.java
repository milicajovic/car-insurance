package com.synechron.carInsurance.Controllers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.carInsurance.Repository.SubscriberRepository;
import com.synechron.carInsurance.Service.CarService;
import com.synechron.carInsurance.Service.InsurancePlanService;
import com.synechron.carInsurance.Service.ProposalService;
import com.synechron.carInsurance.Service.SubscriberService;
import com.synechron.carInsurance.json.CarDTO;
import com.synechron.carInsurance.json.ProposalSubscriberDTO;
import com.synechron.carInsurance.json.SettingLicencePlateDTO;
import com.synechron.carInsurance.model.car.Car;
import com.synechron.carInsurance.model.proposal.InsuranceItem;
import com.synechron.carInsurance.model.proposal.InsurancePlan;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Driver;
import com.synechron.carInsurance.model.users.Subscriber;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@RestController
@RequestMapping("api/v1/proposal")
public class ProposalController {

	
	

	private final ProposalService proposalService;
	private final SubscriberService subscriberService;
	private final CarService carService;
	private final InsurancePlanService ips;
	
	@Autowired
	public ProposalController(ProposalService proposalService, SubscriberService subscriberService, CarService carService, InsurancePlanService ips) {
		this.proposalService = proposalService;
		this.subscriberService = subscriberService;
		this.carService = carService;
		this.ips=ips;
		
	}
	@GetMapping("testMQ/{id}")
	public void testMq(@PathVariable("id") Long id){
		 proposalService.testMQ(id);
	}
	
	@GetMapping
	public List<Proposal> getProposals(){
		return proposalService.getProposals();
	}
	
	@PostMapping("newProposal/{id}")
	public ResponseEntity<ProposalSubscriberDTO> registerNewProposal(@PathVariable("id") Long id) {
	
		
		System.out.println(id);
		Subscriber subscriber = subscriberService.findSubscriberById(id);
		Proposal p =  proposalService.addNewProposal(subscriber);
	
		ProposalSubscriberDTO psd = new ProposalSubscriberDTO(p.getId(), subscriber.getId());
		
		return new ResponseEntity<>(psd, HttpStatus.OK);

	}



	
	@PostMapping("/SetLicencePlate")
	public ResponseEntity<Long> setLicencePlate(@RequestBody SettingLicencePlateDTO setingLicencePlate) {
		System.out.println("++++"+setingLicencePlate.getBrand()+ " " + setingLicencePlate.getModel()+" " + setingLicencePlate.getYear());
		Car c = (Car) carService.getCarWithBrandModelAndYear(setingLicencePlate.getBrand(),setingLicencePlate.getModel(),setingLicencePlate.getYear());
		
		Proposal p = proposalService.updateProposal(Long.parseLong(setingLicencePlate.getProposalID()),setingLicencePlate.getLicencePlateNumber(), c);
		return new ResponseEntity<>(p.getId(),HttpStatus.OK);
		
	}

	@GetMapping("/getProposalById/{proposalID}")
	public Proposal getProposalById(@PathVariable Long proposalID) {
		return proposalService.getProposal(proposalID).get();
	}
	
	@PostMapping("/setCarOnProposal")
	public Proposal setCarOnProposal(@RequestBody CarDTO carDto) {
		
		Car c = carService.getCarWithBrandModelAndYear(carDto.getBrand(), carDto.getModel(), Integer.parseInt(carDto.getYear()));
		return proposalService.updateProposal(carDto,c);
	
	}
	

	
	@GetMapping("driversProposal/{id}")
	public List<Driver> getDrivers(@PathVariable("id") Long id){
		return proposalService.getDriversProposal(id);
	}
	
	@GetMapping("report/{proposalID}")
	public void generateReport(@PathVariable Long proposalID) throws Exception{
		
		Subscriber sub= subscriberService.getSubscriberProposal(proposalID);
		List<Subscriber> subs=new ArrayList<>();
		subs.add(sub);
		
		Car car=carService.getCarProposal(proposalID);
		List<Car>cars= new ArrayList<>();
		cars.add(car);
		
		List<Driver> drivers=proposalService.getDriversProposal(proposalID);
		
		InsurancePlan ip=ips.getInsPlanProposal(proposalID);
		List<InsurancePlan> insurancePlan=new ArrayList<>();
		insurancePlan.add(ip);
		
		List<InsuranceItem> insuranceItems=ips.getInsuranceItems(ip.getId());
		
		
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(subs);
		JRBeanCollectionDataSource dataSourcee = new JRBeanCollectionDataSource(cars);
		JRBeanCollectionDataSource dataSourcee2 = new JRBeanCollectionDataSource(drivers);
		JRBeanCollectionDataSource dataSourcee3 = new JRBeanCollectionDataSource(insurancePlan);
		JRBeanCollectionDataSource dataSourcee4 = new JRBeanCollectionDataSource(insuranceItems);
		
		InputStream inputStream = this.getClass().getResourceAsStream("/jasperreport/proposal.jrxml");
		InputStream inputSubStream = this.getClass().getResourceAsStream("/jasperreport/proposalSubReport.jrxml");
		InputStream inputSubStream2 = this.getClass().getResourceAsStream("/jasperreport/proposalSubReport2.jrxml");
		InputStream inputSubStream3 = this.getClass().getResourceAsStream("/jasperreport/proposalSubReport3.jrxml");
		InputStream inputSubStream4 = this.getClass().getResourceAsStream("/jasperreport/proposalSubReport4.jrxml");
		
		JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
		JasperReport jasperSubReport = JasperCompileManager.compileReport(inputSubStream);
		JasperReport jasperSubReport2 = JasperCompileManager.compileReport(inputSubStream2);
		JasperReport jasperSubReport3 = JasperCompileManager.compileReport(inputSubStream3);
		JasperReport jasperSubReport4 = JasperCompileManager.compileReport(inputSubStream4);
		
		Map<String, Object> parametars = new HashMap<String, Object>();
		
		
		String email ="";
		String homePhone= "";
		String mobilePhone= "";
		String street= "";
		String streetNumber= "";
		String city="";
	
		
		
		email = subs.get(0).getContact().getEmail();
		homePhone =subs.get(0).getContact().getHomePhone();
		mobilePhone=subs.get(0).getContact().getMobilePhone();
		street= subs.get(0).getAddress().street;
		streetNumber= subs.get(0).getAddress().streetNumber;
		city=subs.get(0).getAddress().getCity().getName();
	
	
		parametars.put("email", email);
		parametars.put("homePhone", homePhone);
		parametars.put("mobilePhone", mobilePhone);
		parametars.put("street", street);
		parametars.put("streetNumber", streetNumber);
		parametars.put("city", city);
		parametars.put("car", dataSourcee);
		parametars.put("drivers", dataSourcee2);
		parametars.put("insurancePlan", dataSourcee3);
		parametars.put("insuranceItems", dataSourcee4);
		parametars.put("jasperSubReport", jasperSubReport);
		parametars.put("jasperSubReport2", jasperSubReport2);
		parametars.put("jasperSubReport3", jasperSubReport3);
		parametars.put("jasperSubReport4", jasperSubReport4);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametars, dataSource);
		
		inputStream.close();
	
		JasperExportManager.exportReportToPdfFile(jasperPrint,"proposal.pdf");

		
	}
	
}
