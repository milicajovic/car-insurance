package com.synechron.carInsurance.model.users;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Authority {
	String name;

	public Authority() {
	}

	public Authority(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public String getName() {
		return name;
	}

}