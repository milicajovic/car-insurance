package com.synechron.carInsurance.model.payment;

import com.synechron.carInsurance.model.proposal.Proposal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Policy implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Proposal proposal;

    @Column
    private LocalDateTime dateSigned;

    @Column
    private LocalDateTime moneyReceivedDate;

    @OneToOne
    private Payment payment;

    @Column
    private Double amount;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;
}
