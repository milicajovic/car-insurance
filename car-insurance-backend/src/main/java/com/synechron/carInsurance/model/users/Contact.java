package com.synechron.carInsurance.model.users;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Embeddable
public class Contact {

    @Column
    private String homePhone;

    @Column
    private String  mobilePhone;

    @Column
    private String email;
}
