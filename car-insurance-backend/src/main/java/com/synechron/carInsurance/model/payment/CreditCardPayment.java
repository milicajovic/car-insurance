package com.synechron.carInsurance.model.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class CreditCardPayment{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private CreditCardType creditCardType;

    @Column
    private String creditCardHolder;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;

}
