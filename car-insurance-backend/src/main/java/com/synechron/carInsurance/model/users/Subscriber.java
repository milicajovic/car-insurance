package com.synechron.carInsurance.model.users;

import com.synechron.carInsurance.model.proposal.Proposal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity

public class Subscriber extends Person {

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;

	@OneToOne
	private SubscriberRole subscriberRole;

	@OneToMany
	private List<Proposal> proposalList;

	

	

}
