package com.synechron.carInsurance.model.proposal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class InsuranceItem implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Boolean isOptional;

    @OneToOne
    private Franchise franchise;

    @Column
    private Integer amount;

    @OneToMany
    @JsonIgnore
    private List<Franchise> franchises;

    @ManyToMany
    @JsonIgnore
    private List<Proposal> proposals;

    @ManyToMany
    @JsonIgnore
    private List<InsurancePlan> insurancePlans;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;
}
