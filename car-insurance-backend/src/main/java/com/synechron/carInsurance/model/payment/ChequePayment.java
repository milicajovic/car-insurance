package com.synechron.carInsurance.model.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class ChequePayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String chequeNo;

    @Column
    private LocalDateTime chequeDate;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;

}
