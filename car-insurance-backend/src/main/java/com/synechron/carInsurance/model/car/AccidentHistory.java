package com.synechron.carInsurance.model.car;

import com.synechron.carInsurance.model.users.Driver;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class AccidentHistory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Driver driver;

    @Column(name="time_happened")
    private LocalDate happened;

    @Column(name="was_responsible")
    private Boolean wasResponsible = false;

    @Column
    private String description;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;
}
