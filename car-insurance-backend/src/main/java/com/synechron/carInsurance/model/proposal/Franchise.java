package com.synechron.carInsurance.model.proposal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.synechron.carInsurance.model.proposal.InsuranceItem;
import com.synechron.carInsurance.model.proposal.Proposal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Franchise implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "integer default 0")
    private int percentage;

    @OneToOne
    @JsonIgnore
    private Proposal proposal;

    @OneToOne
    @JsonIgnore
    private InsuranceItem insuranceItem;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;
}
