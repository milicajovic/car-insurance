package com.synechron.carInsurance.model.proposal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.synechron.carInsurance.model.car.Car;
import com.synechron.carInsurance.model.payment.Policy;
import com.synechron.carInsurance.model.users.Driver;
import com.synechron.carInsurance.model.users.Subscriber;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Proposal implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, columnDefinition = "boolean default true")
	private Boolean isValid;

	@Column
	private LocalDateTime creationDate;

	@Column
	private Double amount;
	@Column
	private String licenceNum;

	@ManyToOne
	private Subscriber subscriber;

	@OneToOne
	private InsurancePlan insurancePlan;

	@ManyToOne(cascade = CascadeType.ALL)
	private Car car;

	@OneToOne
	@JsonIgnore
	private Policy policy;

	@ManyToMany
	private List<InsuranceItem> insuranceItems;

	@OneToMany
	private List<Franchise> franchises;

	@ManyToMany
	private List<Driver> drivers;

	@Column(nullable = false, columnDefinition = "boolean default false")
	private Boolean isDeleted = false;
	
	public void addFranchise(Franchise franchise) {
		franchises.add(franchise);
	}

}
