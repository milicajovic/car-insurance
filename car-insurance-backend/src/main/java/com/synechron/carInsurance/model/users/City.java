package com.synechron.carInsurance.model.users;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class City implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private int zipcode;

    @ManyToOne(cascade = CascadeType.ALL)
    private Country country;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;

}
