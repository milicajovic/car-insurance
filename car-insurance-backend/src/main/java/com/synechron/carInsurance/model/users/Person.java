package com.synechron.carInsurance.model.users;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@MappedSuperclass

public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    public String firstName;

    @Column
    private String lastName;

    @ManyToOne
    private MaritalStatus maritalStatus;

    @ManyToOne
    private Gender gender;

    @Column
    private String jmbg;

    @Column
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birth;

    @Embedded
    private Contact contact;

    @ManyToOne(cascade = CascadeType.ALL)
    public Address address;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;

}
