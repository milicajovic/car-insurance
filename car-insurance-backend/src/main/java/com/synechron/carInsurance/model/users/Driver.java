package com.synechron.carInsurance.model.users;

import com.synechron.carInsurance.model.car.AccidentHistory;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.car.Risk;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Driver extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String licenceNum;

    @Column
    private LocalDate licenceObtained;

    @Column
    private Integer yearsInsured;

    @ManyToMany
    private List<Proposal> proposal;

    @ManyToMany
    private List<Risk> risks;

    @OneToMany
    private List<AccidentHistory> accidentHistories;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;
    
    public String getHomePhone() {
    	return super.getContact().getHomePhone();
    }
    public String getMobilePhone() {
    	return super.getContact().getMobilePhone();
    }
    public Long getGenderID() {
    	return super.getGender().getId();
    }
    public String getEmail() {
    	return super.getContact().getEmail();
    }
    public Long getMaritalStatusID() {
    	return super.getMaritalStatus().getId();
    }
    public Long getAddressID() {
    	return super.getAddress().getId();
    }
	@Override
	public String toString() {
		return "Driver [licenceNum=" + licenceNum + "]";
	}
    

}
