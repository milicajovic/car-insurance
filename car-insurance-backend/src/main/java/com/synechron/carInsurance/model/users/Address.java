package com.synechron.carInsurance.model.users;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Address implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    public String street;

    @Column
    public String streetNumber;

    @OneToOne
    @JoinColumn(name="city_id")
    public City city;

    @Column( columnDefinition = "boolean default false")
    private Boolean isDeleted=false;

    public Address(String street, City city){
        this.street=street;
        this.city=city;

    }
    
    public Address() {
    	
    }

}
