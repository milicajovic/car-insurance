package com.synechron.carInsurance.model.proposal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class InsurancePlan implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Boolean isPremium;

    @Column(columnDefinition = "double default 0")
    private Double amount;

    @ManyToMany
    private List<InsuranceItem> insuranceItems;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean isDeleted=false;
    
    public boolean hasItem(Long itemID) {
    	for (InsuranceItem it : insuranceItems) {
    		if ( it.getId() == itemID) {
    			return true;
    		}
    	}
    	return false;
    }


}
