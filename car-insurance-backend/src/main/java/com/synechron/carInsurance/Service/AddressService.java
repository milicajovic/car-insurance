package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.AddressRepository;
import com.synechron.carInsurance.model.users.Address;

@Service
public class AddressService {

	private final AddressRepository adrRepository;

	@Autowired
	public AddressService(AddressRepository adrRepository) {
		this.adrRepository = adrRepository;
	}

	public Address findAddressByID(Long id) {
		return adrRepository.getById(id);
	}
	public List<Address> getAddressesOfCities(Long cityId) {
		return adrRepository.findAllAddressesOfCity(cityId);
	}
}
