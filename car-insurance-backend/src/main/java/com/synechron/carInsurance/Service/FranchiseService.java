package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.FranchiseRepository;
import com.synechron.carInsurance.Repository.InsuranceItemRepository;
import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.json.FranchiseDTO;
import com.synechron.carInsurance.model.proposal.Franchise;
import com.synechron.carInsurance.model.proposal.InsuranceItem;
import com.synechron.carInsurance.model.proposal.Proposal;

@Service
public class FranchiseService {
	private FranchiseRepository franchiseRepository;
	private final ProposalRepository proposalRepository;
	private InsuranceItemRepository insuranceItemRepository;

	@Autowired
	public FranchiseService(FranchiseRepository franchiseRepository,ProposalRepository proposalRepository,InsuranceItemRepository insuranceItemRepository) {
		this.franchiseRepository = franchiseRepository;
		this.proposalRepository = proposalRepository;
		this.insuranceItemRepository = insuranceItemRepository;
	}

	public List<Franchise> getFranchises() {
		return franchiseRepository.findAll();
	}

	public Franchise addFranchise(int percentage,Proposal p,InsuranceItem ii) {
		Franchise f = new Franchise();
		f.setPercentage(percentage);
		f.setProposal(p);
		f.setInsuranceItem(ii);
		franchiseRepository.save(f);
		p.addFranchise(f);
		proposalRepository.save(p);
		ii.setFranchise(f);
		insuranceItemRepository.save(ii);
		return f;
	}

}
