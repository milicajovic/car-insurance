package com.synechron.carInsurance.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.AccidentHistoryRepository;
import com.synechron.carInsurance.model.car.AccidentHistory;



@Service
public class AccidentHistoryService {
	
	private final AccidentHistoryRepository ahr;
	
	@Autowired
	public AccidentHistoryService(AccidentHistoryRepository ahr) {
		this.ahr = ahr;
	}
	
	public AccidentHistory newAccidentHistory(AccidentHistory accidenthistory) {
		return ahr.save(accidenthistory);
	}

}

