package com.synechron.carInsurance.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.BrandRepository;
import com.synechron.carInsurance.Repository.CarRepository;
import com.synechron.carInsurance.Repository.ModelRepository;
import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.json.CarDTO;
import com.synechron.carInsurance.model.car.Brand;
import com.synechron.carInsurance.model.car.Car;
import com.synechron.carInsurance.model.car.Model;
import com.synechron.carInsurance.model.proposal.Proposal;

@Service
public class CarService {

	private final CarRepository carRepository;
	private  BrandRepository brandRepository;
	private ModelRepository modelRepository;
	private ProposalRepository proposalRepository;
	
	@Autowired
	public CarService(CarRepository carRepository,BrandRepository brandRepository
			,ModelRepository modelRepository, ProposalRepository proposalRepository) {
		this.carRepository = carRepository;
		this.brandRepository = brandRepository;
		this.modelRepository = modelRepository;
		this.proposalRepository=proposalRepository;
	}
	
	public CarService() {
		this.carRepository = null;
	}
	
	public List<Car> getCars(){
		return carRepository.findAll();
	}
	
	public List<Car> getCarsPageable(int page, int pageSize){
		Pageable pageable = PageRequest.of(page, pageSize);
		return carRepository.findAllPageable(pageable);
	}
	
	public List<Car> getCarsWithBrand(String brand ){
		return carRepository.findAllCarsWithBrand(brand);
	}
	
	public List<Car> getCarsWithBrandAndModel(String brand, String model ){
		return carRepository.findAllCarsWithBrandAndModel(brand,model);
	}
	
	public Car getCarWithBrandModelAndYear(String brand, String model,Integer year ){
		System.out.println("service---" +brand+ " "+ model+ " "+ year);
		Car c = carRepository.findAllCarsWithBrandAndModelAndYear(brand,model,year);
		System.out.println(c);
		return c;
	}
	
	public Car addNewCar(Car car) {
		if ( car.getBrand() != null && car.getModel() != null && car.getYear() != null) {
			return carRepository.save(car);
		}
		return null;
	}
	
	public Car updateCar(Long carId, CarDTO car) {
		Optional<Car> carr = carRepository.findById(carId);
		Car carrr = carr.get();
		System.out.println("AAAAAAAAAAA");
		carrr.setBrand(car.getBrand());
		carrr.setModel(car.getModel());
		carrr.setYear(Integer.parseInt(car.getYear()));
		System.out.println("AAAAAAAAAAA");

		return carRepository.save(carrr);
	}
	
	public Car deleteCar(Long carId) {
		Car c = carRepository.findById(carId).get();
		 carRepository.deleteById(carId);
		 return c;
	}
	
	public List<String> getBrands(){
		List<Car> cars =  carRepository.findAll();
		Set<String> brandsSet = new HashSet();
		for ( Car c : cars ) {
			brandsSet.add(c.getBrand());
		}
		ArrayList<String> brandsList = new ArrayList<>();
		for ( String s : brandsSet) {
			brandsList.add(s);
		}
		System.out.println("-----"+brandsList+"-----");
		return brandsList;
	}
	
	public List<String> getModels(){
		List<Car> cars =  carRepository.findAll();
		Set<String> modelsSet = new HashSet();
		for ( Car c : cars ) {
			modelsSet.add(c.getModel());
		}
		ArrayList<String> modelsList = new ArrayList<>();
		for ( String s : modelsSet) {
			modelsList.add(s);
		}
		return modelsList;
	}
	
	public List<Car> searchCars(CarDTO car, int page, int pageSize){
		Pageable pageable = PageRequest.of(page, pageSize);
		
		if(car.getBrand() != null && car.getModel() != null && car.getYear() != null) {
			if(car.getBrand().length() >= 3 && car.getModel().length() >= 3 && car.getYear().length() == 4) {
				return this.carRepository.findAllCarsWithBrandAndModelAndYearPageable
						(car.getBrand(), car.getModel(), Integer.parseInt(car.getYear()), pageable);
			}else if(car.getBrand().length() >= 3 && car.getModel().length() >= 3 && car.getYear().length() != 4) {
				return this.carRepository.findByBrandAndModelContaining(car.getBrand(), car.getModel(), pageable);
			}else if(car.getBrand().length() >= 3 && car.getModel().length() < 3 && car.getYear().length() == 4) {
				return this.carRepository.findByBrandAndYearContaining(car.getBrand(),Integer.parseInt(car.getYear()), pageable);
			}else if(car.getBrand().length() < 3 && car.getModel().length() >= 3 && car.getYear().length() == 4) {
				return this.carRepository.findByModelAndYearContaining(car.getModel(), Integer.parseInt(car.getYear()), pageable);
			}else if(car.getBrand().length() >= 3 && car.getModel().length() < 3 && car.getYear().length() != 4) {
				return this.carRepository.findByBrandContaining(car.getBrand(), pageable);
			}else if(car.getBrand().length() < 3 && car.getModel().length() >= 3 && car.getYear().length() != 4) {
				return this.carRepository.findByModelContaining(car.getModel(), pageable);
			}else if(car.getBrand().length() < 3 && car.getModel().length() < 3 && car.getYear().length() == 4) {
				return this.carRepository.findByYear(Integer.parseInt(car.getYear()), pageable);
			}
	}
		return new ArrayList<Car>();
	}

	
	public Car getCarProposal(Long id) {
		Proposal p=proposalRepository.findById(id).get();
		
		if ( p == null ) {
			return null;
		}
		return  p.getCar();
	}
}
