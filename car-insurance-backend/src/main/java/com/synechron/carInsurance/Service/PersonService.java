package com.synechron.carInsurance.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.AddressRepository;
import com.synechron.carInsurance.Repository.DriverRepository;
import com.synechron.carInsurance.Repository.GenderRepository;
import com.synechron.carInsurance.Repository.MaritalStatusRepository;
import com.synechron.carInsurance.Repository.SubscriberRepository;
import com.synechron.carInsurance.json.PersonDTO;
import com.synechron.carInsurance.model.users.Address;
import com.synechron.carInsurance.model.users.Contact;
import com.synechron.carInsurance.model.users.Driver;
import com.synechron.carInsurance.model.users.Subscriber;

@Service
public class PersonService {
	private SubscriberRepository subscriberRepository;
	private DriverRepository driverRepository;
	private GenderRepository genderRepository;
	private MaritalStatusRepository maritalStatusRepository;
	private AddressRepository addressRepository;
	
	public PersonService(SubscriberRepository subscriberRepository, DriverRepository driverRepository, GenderRepository genderRepository, MaritalStatusRepository
			maritalStatusRepository, AddressRepository addressRepository) {
		this.subscriberRepository = subscriberRepository;
		this.driverRepository = driverRepository;
		this.genderRepository = genderRepository;
		this.maritalStatusRepository = maritalStatusRepository;
		this.addressRepository = addressRepository;
	}
	
	public List<PersonDTO> getAll(int page, int pageSize) {
		Pageable pageable = PageRequest.of(page, pageSize);
		List<Object[]> list = this.subscriberRepository.getAllMerged(pageable);
		List<PersonDTO> l = new ArrayList<PersonDTO>();
		for(Object[] arr : list) {
			PersonDTO p = new PersonDTO(String.valueOf(arr[0]), String.valueOf(arr[1]),
					Long.parseLong((String.valueOf(arr[2]))),Long.parseLong(String.valueOf(arr[3])),
					String.valueOf(arr[4]), null, String.valueOf(arr[6]),String.valueOf(arr[7]),
					String.valueOf(arr[8]), Long.parseLong(String.valueOf(arr[9])), null, null, null);
			Address a = this.addressRepository.findById(Long.parseLong(String.valueOf(arr[9]))).get();
			p.setCity(a.getCity().getName());
			p.setStreetName(a.getStreet());
			p.setStreetNumber(a.getStreetNumber());
			p.setBirth(LocalDate.parse(String.valueOf(arr[5])));
			l.add(p);
		}
		return l;
	}
	
	public void editPerson(PersonDTO person) {
		Optional<Subscriber> s = this.subscriberRepository.findSubscriberByJmbg(person.getJmbg());
		Optional<Driver> d = this.driverRepository.findDriverByJmbg(person.getJmbg());
		Subscriber ss = null;
		Driver dd = null;
		
		if(s.isPresent()) {
			ss = s.get();
			ss.setFirstName(person.getFirstName());
			ss.setLastName(person.getLastName());
			ss.setMaritalStatus(this.maritalStatusRepository.findById(person.getMaritalStatus()).get());
			ss.setGender(this.genderRepository.findById(person.getGender()).get());
			ss.setJmbg(person.getJmbg());
			ss.setBirth(person.getBirth());
			ss.setContact(new Contact(person.getEmail(), person.getHomePhone(), person.getMobilePhone()));
			
			if(person.getStreetNumber() == null)
				ss.setAddress(this.addressRepository.findAddressByCityStreet(person.getCity(), person.getStreetName()).get());
			else
				ss.setAddress(this.addressRepository.findAddressByCityStreetNumber(person.getCity(), person.getStreetName(), person.getStreetNumber()).get());
		
			this.subscriberRepository.save(ss);
		}
		
		if(d.isPresent()) {
			dd = d.get();
			dd.setFirstName(person.getFirstName());
			dd.setLastName(person.getLastName());
			dd.setMaritalStatus(this.maritalStatusRepository.findById(person.getMaritalStatus()).get());
			dd.setGender(this.genderRepository.findById(person.getGender()).get());
			dd.setJmbg(person.getJmbg());
			dd.setBirth(person.getBirth());
			dd.setContact(new Contact(person.getEmail(), person.getHomePhone(), person.getMobilePhone()));
			dd.setAddress(this.addressRepository.findById(person.getAddress()).get());
			
			this.driverRepository.save(dd);
		}
	}
	
	public void deletePerson(String jmbg) {
		Optional<Subscriber> s = this.subscriberRepository.findSubscriberByJmbg(jmbg);
		Optional<Driver> d = this.driverRepository.findDriverByJmbg(jmbg);
		Subscriber ss = null;
		Driver dd = null;
		
		if(d.isPresent()) {
			dd = d.get();
			this.driverRepository.deleteById(dd.getId());
		}
		
		if(s.isPresent()) {
			ss = s.get();
			this.subscriberRepository.deleteById(ss.getId());
		}
	}
	
	public List<PersonDTO> searchPeople(String firstName, String lastName, String jmbg, int page, int pageSize){
		Pageable pageable = PageRequest.of(page, pageSize);
		List<Object[]> list = null;
		List<PersonDTO> l = new ArrayList<PersonDTO>();
		
		if(firstName != null && lastName != null && jmbg != null) {
			if(firstName.length() >= 3 && lastName.length() >= 3 && jmbg.length() >= 5) {
				list = this.subscriberRepository.getAllMergedFirstNameLastNameJmbg(firstName, lastName, jmbg, pageable);
			}else if(firstName.length() >= 3 && lastName.length() >= 3 && jmbg.length() < 5) {
				list = this.subscriberRepository.getAllMergedFirstNameLastName(firstName, lastName, pageable);
			}else if(firstName.length() >= 3 && lastName.length() < 3 && jmbg.length() >= 5) {
				list = this.subscriberRepository.getAllMergedFirstNameJmbg(firstName, jmbg, pageable);
			}else if(firstName.length() < 3 && lastName.length() >= 3 && jmbg.length() >= 5) {
				list = this.subscriberRepository.getAllMergedLastNameJmbg(lastName, jmbg, pageable);
			}else if(firstName.length() >= 3 && lastName.length() < 3 && jmbg.length() < 5) {
				list = this.subscriberRepository.getAllMergedFirstName(firstName, pageable);
			}else if(firstName.length() < 3 && lastName.length() >= 3 && jmbg.length() < 5) {
				list = this.subscriberRepository.getAllMergedLastName(lastName, pageable);
			}else if(firstName.length() < 3 && lastName.length() < 3 && jmbg.length() >= 5) {
				list = this.subscriberRepository.getAllMergedJmbg(jmbg, pageable);
			}
			
			for(Object[] arr : list) {
				PersonDTO p = new PersonDTO(String.valueOf(arr[0]), String.valueOf(arr[1]),
						Long.parseLong((String.valueOf(arr[2]))),Long.parseLong(String.valueOf(arr[3])),
						String.valueOf(arr[4]), null, String.valueOf(arr[6]),String.valueOf(arr[7]),
						String.valueOf(arr[8]), Long.parseLong(String.valueOf(arr[9])), null, null, null);
				Address a = this.addressRepository.findById(Long.parseLong(String.valueOf(arr[9]))).get();
				p.setCity(a.getCity().getName());
				p.setStreetName(a.getStreet());
				p.setStreetNumber(a.getStreetNumber());
				p.setBirth(LocalDate.parse(String.valueOf(arr[5])));
				l.add(p);
			}			
			return l;
		}
		return null;
	}
}
