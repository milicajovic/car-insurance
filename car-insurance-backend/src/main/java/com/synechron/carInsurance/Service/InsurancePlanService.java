package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.FranchiseRepository;
import com.synechron.carInsurance.Repository.InsuranceItemRepository;
import com.synechron.carInsurance.Repository.InsurancePlanRepository;
import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.json.InsuranceItemDTO;
import com.synechron.carInsurance.json.InsurancePlanDTO;
import com.synechron.carInsurance.model.proposal.Franchise;
import com.synechron.carInsurance.model.proposal.InsuranceItem;
import com.synechron.carInsurance.model.proposal.InsurancePlan;
import com.synechron.carInsurance.model.proposal.Proposal;



@Service
public class InsurancePlanService {
	
	private InsurancePlanRepository insurancePlanRepository;
	private InsuranceItemRepository insuranceItemRepository;
	private ProposalRepository proposalRepository;
	private FranchiseRepository franchiseRepository;
	
	@Autowired
	public InsurancePlanService(InsurancePlanRepository insurancePlanRepository,InsuranceItemRepository insuranceItemRepository
			,ProposalRepository proposalRepository, FranchiseRepository franchiseRepository) {
		this.insurancePlanRepository = insurancePlanRepository;
		this.insuranceItemRepository = insuranceItemRepository;
		this.proposalRepository = proposalRepository;
		this.franchiseRepository = franchiseRepository;
	}
	
	public List<InsurancePlan> getInsurancePlans() {
		return insurancePlanRepository.findAll();
	}
	public List<InsuranceItem> getInsuranceItems(Long id) {
		InsurancePlan ip = insurancePlanRepository.findById(id).get();
		if ( ip == null ) {
			return null;
		}
		return ip.getInsuranceItems();
	}
	public List<InsuranceItem> getAllInsuranceItems() {
		return  insuranceItemRepository.findAll();
		
	}

	public InsurancePlan addInsuranceItem(Long itemID, Long planID) {
	
		InsurancePlan ip = insurancePlanRepository.findById(planID).get();
		if ( ip.hasItem(itemID)) {
			return null;
		}
		System.out.println(ip.getId()+"serviccc");
		if (!ip.getIsPremium() ) {
			System.out.println("prem");
			return null;
		}
		if ( ip != null ) {
			ip.getInsuranceItems().add(insuranceItemRepository.findById(itemID).get());
			System.out.println(insuranceItemRepository.findById(itemID).get()+"servicccitem");
			return insurancePlanRepository.save(ip);
		}
		return null;
	}

	public Double getPriceOfInsurancePlan(Long id) {
		InsurancePlan ip = insurancePlanRepository.findById(id).get();
		double price = 0.0;
		if ( ip != null ) {
			for ( InsuranceItem ii : ip.getInsuranceItems()) {
				
				if ( ii.getFranchise() != null ) {
					System.out.println("fransiza"+ii.getFranchise().getPercentage() );
				price = price + ii.getAmount()/100*ii.getFranchise().getPercentage();
				} else {
					price = price + ii.getAmount();
				}
			}
			ip.setAmount(price);
			insurancePlanRepository.save(ip);
			return price;
		}
		return null;
	}
	public Proposal setPlanOnProposal(InsurancePlanDTO plan,Long idProposal) {
		Proposal p = proposalRepository.findById(idProposal).get();
		if ( p!= null ) {
			InsurancePlan ip = insurancePlanRepository.getById(plan.getId());
			for ( InsuranceItem ii : ip.getInsuranceItems()) {
				for (InsuranceItemDTO iid : plan.getInsuranceItems()) {
					if (ii.getId() == iid.getId()) {
						Franchise f = new Franchise();
						if (iid.getFranchise() != 0 ) {
							f.setPercentage(iid.getFranchise());
						} else {
							f.setPercentage(100);
						}
						f.setProposal(p);
						f.setInsuranceItem(ii);
						ii.setFranchise(f);
						franchiseRepository.save(f);
						insuranceItemRepository.save(ii);
					}
				}
			}
			getPriceOfInsurancePlan(plan.getId());
			p.setInsurancePlan(ip);
			proposalRepository.save(p);
			return p;
		}
		return null;
	}


	
	public InsurancePlan getInsPlanProposal(Long id) {
		Proposal p=proposalRepository.findById(id).get();
		
		if ( p == null ) {
			return null;
		}
		return p.getInsurancePlan();
	}

	

}
