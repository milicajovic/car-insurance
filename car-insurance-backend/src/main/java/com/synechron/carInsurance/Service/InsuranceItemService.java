package com.synechron.carInsurance.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.InsuranceItemRepository;
import com.synechron.carInsurance.Repository.InsurancePlanRepository;
import com.synechron.carInsurance.model.proposal.InsuranceItem;

@Service
public class InsuranceItemService {
	private InsuranceItemRepository insuranceItemRepository;
	
	@Autowired
	public InsuranceItemService(InsuranceItemRepository insuranceItemRepository) {
		this.insuranceItemRepository = insuranceItemRepository;
	}
	
	public InsuranceItem getItemById(Long id) {
		return insuranceItemRepository.findById(id).get();
	}
}
