package com.synechron.carInsurance.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.Repository.SubscriberRepository;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Subscriber;

@Service
public class SubscriberService {

	private final SubscriberRepository subscriberRepository;
	private final AddressService adrService;
	private final ProposalRepository proposalRepository;

	@Autowired
	public SubscriberService(SubscriberRepository subscriberRepository, AddressService adrService, ProposalRepository proposalRepository) {
		this.subscriberRepository = subscriberRepository;
		this.adrService = adrService;
		this.proposalRepository=proposalRepository;
	}

	public List<Subscriber> getSubscribers() {
		return subscriberRepository.findAll();
	}

	public Subscriber addSubscriber(Subscriber subscriber) {
			return subscriberRepository.save(subscriber);
	}
	
	public Subscriber findSubscriberByJmbg(String jmbg) {
		Optional<Subscriber> sub = subscriberRepository.findSubscriberByJmbg(jmbg);
		Subscriber subscriber = new Subscriber();
		if(sub.isPresent()) {
			subscriber = sub.get();
		}else {
			throw new IllegalStateException("User with that JMBG does not exist");
		}
		return subscriber;
	}
	
	public Subscriber findSubscriberById(Long id) {
		return subscriberRepository.getById(id);
	}
	
	public boolean subscriberExists(String jmbg) {
		Optional<Subscriber> s = subscriberRepository.findSubscriberByJmbg(jmbg);	
		return s.isPresent();
	}

	
	
	public Subscriber getSubscriberProposal(Long id) {
		Proposal p=proposalRepository.findById(id).get();
		
		if ( p == null ) {
			return null;
		}
		return  p.getSubscriber();
	}
}
