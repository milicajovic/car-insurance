package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.GenderRepository;
import com.synechron.carInsurance.model.users.Gender;

@Service
public class GenderService {

	private final GenderRepository genderRepository;
	public GenderService(GenderRepository genderRepository) {
		this.genderRepository = genderRepository;
	}
	
	public Gender findGenderById(Long id) {
		return genderRepository.getById(id);
	}
	public List<Gender> getGenders() {
		return genderRepository.findAll();
	}
}
