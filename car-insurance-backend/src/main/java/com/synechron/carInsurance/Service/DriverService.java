package com.synechron.carInsurance.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.DriverRepository;
import com.synechron.carInsurance.model.users.Driver;





@Service
public class DriverService {
	
	private final DriverRepository driverRepository;
	
	@Autowired
	public DriverService(DriverRepository driverRepository) {
		this.driverRepository = driverRepository;
	}

	public List<Driver> getDrivers(){
		return driverRepository.findAll();
	}
	
	public Driver findDriverByJmbg(String jmbg) {
		Optional<Driver> drvr = driverRepository.findDriverByJmbg(jmbg);
		Driver driver = new Driver();
		if(drvr.isPresent()) {
			driver = drvr.get();
		}else {
			return null;
		}
		return driver;
	}
	
	public Driver addDriver(Driver driver) {
		return driverRepository.save(driver);
	}
	
	
	
	

}

