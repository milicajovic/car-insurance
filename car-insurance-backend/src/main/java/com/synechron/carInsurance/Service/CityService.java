package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.CityRepository;
import com.synechron.carInsurance.model.users.City;



@Service
public class CityService {
	
	@Autowired
	private CityRepository cityRepository;
	
	public List<City> getCitiesOfCountry(Long countryId) {
		return cityRepository.findAllCitiesOfCountry(countryId);
	}
}
