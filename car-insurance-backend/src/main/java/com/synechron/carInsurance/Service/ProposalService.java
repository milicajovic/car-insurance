package com.synechron.carInsurance.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.synechron.carInsurance.CarInsuranceBackEndApplication;
import com.synechron.carInsurance.Repository.DriverRepository;
import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.json.CarDTO;
import com.synechron.carInsurance.json.SettingLicencePlateDTO;
import com.synechron.carInsurance.model.car.Car;
import com.synechron.carInsurance.model.proposal.InsuranceItem;
import com.synechron.carInsurance.model.proposal.InsurancePlan;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Driver;
import com.synechron.carInsurance.model.users.Subscriber;

@Service
public class ProposalService {
	
	
	private final MessagingService messagingService;
	private final ProposalRepository proposalRepository;
	
	@Autowired
	public ProposalService(ProposalRepository proposalRepository,MessagingService messagingService) {
		this.proposalRepository = proposalRepository;
		this.messagingService = messagingService;
		
	}
	
	public List<Proposal> getProposals(){
		return proposalRepository.findAll();
	}
	public Optional<Proposal> getProposal(Long id){
		return proposalRepository.findById(id);
		
	}
	public Proposal updateProposal(Long proposalID,String licencePlateNumber, Car c){
		Proposal p = getProposal(proposalID).get();
		p.setLicenceNum(licencePlateNumber);
		p.setCar(c);
		System.out.println("-+-+-+"+licencePlateNumber+"-+-+-+"+
		proposalID+"-+-+-+"+c.getBrand()+"-+-+-+"+c.getModel()+"-+-+-+"+c.getYear()+"-+-+-+"+c.getId()+"-+-+-+");
		return proposalRepository.save(p);
	}
	public Proposal updateProposal(CarDTO carDto, Car car){

		Proposal p = proposalRepository.getById(Long.parseLong(carDto.getProposalId()));
		p.setCar(car);
		p.setLicenceNum(carDto.getLicencePlate());
		return proposalRepository.save(p);
	}
	public void testMQ(Long id) {
		messagingService.sendMessage(id);
	}
	
	public Proposal addNewProposal(Subscriber subscriber) {
		Proposal proposal = new Proposal();
		proposal.setSubscriber(subscriber);
		proposal.setIsValid(false);
		System.out.println("service debugging " + proposal);
		
		return proposalRepository.save(proposal);
	}
	public void addNewProposal(Proposal proposal) {
		if(proposal.getCar() != null) {
			Optional<Proposal> propOptional = proposalRepository.findProposalByCarId(proposal.getCar().getId());
			if(propOptional.isPresent()) {
				throw new IllegalStateException("Car already has a proposal");
			}
		}
		proposalRepository.save(proposal);
	}
	public Proposal addDriversOnProposal(List<Driver> drivers, Long proposalId) {
		Proposal p =  getProposal(proposalId).get();
		if ( p == null ) {
			return null;
		}
		p.setDrivers(drivers);
	
		return proposalRepository.save(p);
	}
	

	
	public List<Driver> getDriversProposal(Long id) {
		Proposal p=proposalRepository.findById(id).get();
		
		if ( p == null ) {
			return null;
		}
		return p.getDrivers();
	}


	
}
