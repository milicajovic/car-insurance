package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.MaritalStatusRepository;
import com.synechron.carInsurance.model.users.MaritalStatus;

@Service
public class MaritalStatusService {
	
	private final MaritalStatusRepository maritalStatusRepository;
	
	@Autowired
	public MaritalStatusService (MaritalStatusRepository maritalStatusRepository) {
		this.maritalStatusRepository = maritalStatusRepository;
	}
	
	public MaritalStatus findMaritalStatusById(Long id) {
		return maritalStatusRepository.getById(id);
	}
	public List<MaritalStatus> getMaritalStatuses() {
		return maritalStatusRepository.findAll();
	}

}
