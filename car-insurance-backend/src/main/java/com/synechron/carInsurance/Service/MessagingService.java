package com.synechron.carInsurance.Service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.CarInsuranceBackEndApplication;
import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.model.proposal.Proposal;

@Service
@Component
public class MessagingService {
	
	private JmsTemplate jmsTemplate;
	private ProposalRepository proposalRepository;
	
	@Autowired
	public MessagingService(ProposalRepository proposalRepository,JmsTemplate jmsTemplate) {
		this.proposalRepository = proposalRepository;
		this.jmsTemplate = jmsTemplate;
	}
	
	 public void sendMessage(Long id) {
	        Map<String, String> actionmap = new HashMap<>();	        
	        Proposal p = proposalRepository.getById(id);
	        actionmap.put("proposal", id.toString());
	        String email = p.getSubscriber().getContact().getEmail();
	        actionmap.put("email", email);
	        System.out.println("Sending the index request through queue message" 
	        + actionmap.get("proposal") + " " + actionmap.get("email"));
	        jmsTemplate.convertAndSend(CarInsuranceBackEndApplication.RECEIPT_MESSAGE, actionmap);
	    }
}
