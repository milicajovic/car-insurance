package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.synechron.carInsurance.Repository.RiskRepository;
import com.synechron.carInsurance.model.car.Risk;

@Service
public class RiskService {
	
	private final RiskRepository riskRepository;
	
	@Autowired
	public RiskService(RiskRepository riskRepository) {
		this.riskRepository = riskRepository;
	}
	
	public List<Risk> allRisks(){
		return riskRepository.findAll();
	}

}
