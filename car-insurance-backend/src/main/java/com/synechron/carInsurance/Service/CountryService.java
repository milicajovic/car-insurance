package com.synechron.carInsurance.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.CountryRepository;
import com.synechron.carInsurance.model.users.Country;

@Service
public class CountryService {
	
	@Autowired
	CountryRepository countryRepository;
	
	public List<Country> getCountries() {
		return countryRepository.findAll();
	}
}
