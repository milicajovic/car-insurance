package com.synechron.carInsurance.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.ProposalRepository;
import com.synechron.carInsurance.model.proposal.Proposal;

import java.util.Map;
import java.util.Properties;


@Service
public class EmailService {

	private final ProposalRepository proposalRepository;
    
	@Autowired
	public EmailService(ProposalRepository proposalRepository) {
		this.proposalRepository = proposalRepository;
	}
    public void sendEmail(Map<String, String> messageMap) {
    	
    	 final String username = "sekerusratko@gmail.com";
         final String password = "slcg ipxy jrgw ujug";

         Properties prop = new Properties();
 		 prop.put("mail.smtp.host", "smtp.gmail.com");
         prop.put("mail.smtp.port", "587");
         prop.put("mail.smtp.auth", "true");
         prop.put("mail.smtp.starttls.enable", "true"); //TLS
         prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
         Session session = Session.getInstance(prop,
                 new javax.mail.Authenticator() {
                     protected PasswordAuthentication getPasswordAuthentication() {
                         return new PasswordAuthentication(username, password);
                     }
                 });
        
         try {

             Message message = new MimeMessage(session);
             message.setFrom(new InternetAddress("sekerusratko@gmail.com"));
             message.setRecipients(
                     Message.RecipientType.TO,
                     InternetAddress.parse((String) messageMap.get("email"))
             );
             String idP = messageMap.get("proposal");
             
             Proposal p = proposalRepository.getById(Long.parseLong(idP));
             String name = p.getSubscriber().getFirstName();
             message.setSubject("Receipt");
             message.setText("Postovani "+name+","
                     + "\n\n vas racun je ");
           
             MimeBodyPart messageBodyPart = new MimeBodyPart();

             Multipart multipart = new MimeMultipart();
             
             String file = "path of file to be attached";
             String fileName = "attachmentName";
             DataSource source = new FileDataSource(file);
             messageBodyPart.setDataHandler(new DataHandler(source));
             messageBodyPart.setFileName(fileName);
             multipart.addBodyPart(messageBodyPart);

             message.setContent(multipart);
             Transport.send(message);

             System.out.println("Done");

         } catch (MessagingException e) {
             e.printStackTrace();
         }
    } 


}
