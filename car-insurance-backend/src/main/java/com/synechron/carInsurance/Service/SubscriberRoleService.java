package com.synechron.carInsurance.Service;

import org.springframework.stereotype.Service;

import com.synechron.carInsurance.Repository.SubscriberRoleRepository;
import com.synechron.carInsurance.model.users.SubscriberRole;

@Service
public class SubscriberRoleService {

	private final SubscriberRoleRepository subscriberRoleService;
	
	public SubscriberRoleService(SubscriberRoleRepository subscriberRoleService) {
		this.subscriberRoleService = subscriberRoleService;
	}
	
	public SubscriberRole findSubscriberRoleById(Long id) {
		return this.subscriberRoleService.getById(id);
	}
}
