package com.synechron.carInsurance.json;

import java.time.LocalDateTime;
import java.util.List;

import com.synechron.carInsurance.model.proposal.Franchise;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProposalDTO {

	private Boolean isValid;
	private LocalDateTime creationDate;
	private Double amount;
	private String licenceNum;
	private Long subscriber;
	private Long insurancePlan;
	private Long car;
	private Long policy;
	private List<Long> insuranceItems;
	private List<Franchise> franchises;
	private List<Long> drivers;
	private Boolean isDeleted = false;
}
