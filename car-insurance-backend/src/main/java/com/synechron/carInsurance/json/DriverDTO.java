package com.synechron.carInsurance.json;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synechron.carInsurance.model.car.AccidentHistory;
import com.synechron.carInsurance.model.car.Risk;
import com.synechron.carInsurance.model.proposal.Proposal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DriverDTO {
	
	 private String firstName;
	 private String lastName;
	 private Long maritalStatus;
	 private Long gender;
	 private String jmbg;
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	 private LocalDate birth;
	 private String email;
	 private String homePhone;
	 private String mobilePhone;
	 public Long address;
	 private String licenceNum;
	 private Integer yearsInsured;
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	 private LocalDate licenceObtained;
	 private Boolean isDeleted=false;
	 private List<Proposal> proposalList;
	 private List<Risk> risk;
	 private List <AccidentHistory> accidentHistories;

}
