package com.synechron.carInsurance.json;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synechron.carInsurance.model.car.AccidentHistory;
import com.synechron.carInsurance.model.car.Risk;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Driver;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DriversListDTO {
	private List<DriverDTO> drivers;
}
