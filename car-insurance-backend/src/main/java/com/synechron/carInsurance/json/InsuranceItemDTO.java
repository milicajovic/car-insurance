package com.synechron.carInsurance.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceItemDTO {
	private Long id;
	private String name;
	private boolean isOptional;
	private FranchiseDTO franchiseO;
	private int franchise;
	private double amount;
	
}
