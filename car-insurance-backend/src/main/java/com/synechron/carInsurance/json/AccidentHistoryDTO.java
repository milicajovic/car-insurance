package com.synechron.carInsurance.json;

import java.time.LocalDate;


import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccidentHistoryDTO {
	
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	 private LocalDate happened;
	 private Boolean wasResponsible = false;
	 private String description;
	 private Boolean isDeleted=false;
}
