package com.synechron.carInsurance.json;

import java.time.LocalDateTime;
import java.util.List;

import com.synechron.carInsurance.model.proposal.Franchise;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SettingLicencePlateDTO {
	private String brand;
	private String model;
	private Integer year;
	private String licencePlateNumber;
	private String proposalID;
}
