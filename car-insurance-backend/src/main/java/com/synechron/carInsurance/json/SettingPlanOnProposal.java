package com.synechron.carInsurance.json;

import com.synechron.carInsurance.model.proposal.InsurancePlan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SettingPlanOnProposal {
	private InsurancePlanDTO plan;
	private Long idProposal;
}
