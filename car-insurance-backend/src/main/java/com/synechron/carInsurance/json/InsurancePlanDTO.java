package com.synechron.carInsurance.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InsurancePlanDTO {
	private Long id;
	private String name;
	private boolean isPremium;
	private double amount;
	private List<InsuranceItemDTO> insuranceItems;
}
