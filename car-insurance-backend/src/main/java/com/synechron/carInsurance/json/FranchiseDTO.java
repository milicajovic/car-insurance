package com.synechron.carInsurance.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FranchiseDTO {
	private Long proposalID;
	private Long insuranceItemID;
	private int percentage = 100;
}
