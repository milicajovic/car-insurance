package com.synechron.carInsurance.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProposalSubscriberDTO {

	private Long idP;
	private Long idS;
}
