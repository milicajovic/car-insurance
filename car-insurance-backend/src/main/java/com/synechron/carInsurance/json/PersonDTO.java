package com.synechron.carInsurance.json;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synechron.carInsurance.model.proposal.Proposal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersonDTO {

	public String firstName;
	private String lastName;
	private Long maritalStatus;
	private Long gender;
	private String jmbg;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	 private LocalDate birth;
	 private String email;
	 private String homePhone;
	 private String mobilePhone;
	 public Long address;
	 private String city;
	 private String streetName;
	 private String streetNumber;
	 
	@Override
	public String toString() {
		return "PersonDTO [firstName=" + firstName + ", lastName=" + lastName + ", maritalStatus=" + maritalStatus
				+ ", gender=" + gender + ", jmbg=" + jmbg + ", birth=" + birth + ", email=" + email + ", homePhone="
				+ homePhone + ", mobilePhone=" + mobilePhone + ", address=" + address + "]";
	}
	 
	 
}
