package com.synechron.carInsurance.json;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.synechron.carInsurance.model.proposal.Proposal;
import com.synechron.carInsurance.model.users.Address;
import com.synechron.carInsurance.model.users.Contact;
import com.synechron.carInsurance.model.users.Gender;
import com.synechron.carInsurance.model.users.MaritalStatus;
import com.synechron.carInsurance.model.users.SubscriberRole;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SubscriberDTO {

	 public String firstName;
	 private String lastName;
	 private Long maritalStatus;
	 private Long gender;
	 private String jmbg;
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	 private LocalDate birth;
//	 private Contact contact;
	 private String email;
	 private String homePhone;
	 private String mobilePhone;
	 public Long address;
	 private Boolean isDeleted=false;
	 private Long subscriberRole;
	 private List<Proposal> proposalList;
	 
	 
}
