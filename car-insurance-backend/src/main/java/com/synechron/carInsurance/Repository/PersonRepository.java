package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.users.Subscriber;

@Repository
public interface PersonRepository extends JpaRepository<Subscriber, Long> {

}
