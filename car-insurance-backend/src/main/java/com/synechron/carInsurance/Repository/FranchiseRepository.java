package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.carInsurance.model.proposal.Franchise;


public interface FranchiseRepository extends JpaRepository<Franchise, Long> {

}
