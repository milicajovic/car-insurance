package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.carInsurance.model.proposal.InsurancePlan;

public interface InsurancePlanRepository extends JpaRepository<InsurancePlan, Long> {

}
