package com.synechron.carInsurance.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.proposal.Proposal;

@Repository
public interface ProposalRepository extends JpaRepository<Proposal, Long> {

	@Query("SELECT p from Proposal p WHERE p.car.id = ?1")
	Optional<Proposal> findProposalByCarId(Long id);
	
}
