package com.synechron.carInsurance.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.users.Driver;


@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

	Optional<Driver> findDriverByJmbg(String jmbg);
	
	List<Driver> findDriverByFirstNameContaining(String firstName);
	List<Driver> findDriverByLastNameContaining(String lastName);
	List<Driver> findDriverByJmbgContaining(String jmbg);
}
