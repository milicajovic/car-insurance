package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.carInsurance.model.users.Gender;

public interface GenderRepository extends JpaRepository<Gender, Long> {

}
