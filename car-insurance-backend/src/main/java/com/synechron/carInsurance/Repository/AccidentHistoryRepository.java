package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.car.AccidentHistory;

@Repository
public interface AccidentHistoryRepository extends JpaRepository<AccidentHistory, Long>{

}

