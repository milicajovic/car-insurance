package com.synechron.carInsurance.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.synechron.carInsurance.model.users.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {
	

	@Query("SELECT a from Address a WHERE a.city.id = ?1")
	List<Address> findAllAddressesOfCity(Long cityId);
	
	@Query("SELECT a from Address a LEFT JOIN City c on a.city.id = c.id WHERE c.name = ?1 and a.street = ?2 and a.streetNumber = ?3")
	Optional<Address> findAddressByCityStreetNumber(String city, String street, String number);
	
	@Query("SELECT a from Address a LEFT JOIN City c on a.city.id = c.id WHERE c.name = ?1 and a.street = ?2")
	Optional<Address> findAddressByCityStreet(String city, String street);
}
