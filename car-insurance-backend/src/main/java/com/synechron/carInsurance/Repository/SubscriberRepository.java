package com.synechron.carInsurance.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.json.PersonDTO;
import com.synechron.carInsurance.model.users.Subscriber;

@Repository
public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {
	String getAllMerged = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +
			"from Driver";

	String getAllMergedFirstName = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber WHERE first_name LIKE %?1%  UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email" + 
			", home_phone, mobile_phone, address_id " +
			"from Driver WHERE first_name LIKE %?1%";
	
	String getAllMergedLastName = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber WHERE last_name LIKE %?1% UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email" + 
			", home_phone, mobile_phone, address_id " +
			"from Driver  WHERE last_name LIKE %?1%";
	
	String getAllMergedJmbg = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber WHERE jmbg LIKE %?1% UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email" + 
			", home_phone, mobile_phone, address_id " +
			"from Driver  WHERE jmbg LIKE %?1%";
	
	String getAllMergedFirstNameLastName = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber WHERE first_name LIKE %?1% AND last_name LIKE %?2% UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email" + 
			", home_phone, mobile_phone, address_id " +
			"from Driver  WHERE first_name LIKE %?1% AND last_name LIKE %?2%";
	
	String getAllMergedFirstNameJmbg = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber WHERE first_name LIKE %?1% AND jmbg LIKE %?2% UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email" + 
			", home_phone, mobile_phone, address_id " +
			"from Driver  WHERE first_name LIKE %?1% AND jmbg LIKE %?2%";
	
	String getAllMergedLastNameJmbg = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber WHERE last_name LIKE %?1% AND jmbg LIKE %?2% UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email" + 
			", home_phone, mobile_phone, address_id " +
			"from Driver  WHERE last_name LIKE %?1% AND jmbg LIKE %?2%";
	
	String getAllMergedFirstLastJmbg = "select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email, home_phone, mobile_phone, address_id " +  
			"from Subscriber  WHERE first_name LIKE %?1% AND last_name LIKE %?2% AND jmbg LIKE %?3% UNION select first_name, last_name, marital_status_id, gender_id, jmbg, birth, email" + 
			", home_phone, mobile_phone, address_id " +
			"from Driver  WHERE first_name LIKE %?1% AND last_name LIKE %?2% AND jmbg LIKE %?3%";
	
	@Query("SELECT s from Subscriber s WHERE s.jmbg = ?1")
	Optional<Subscriber> findSubscriberByJmbg(String jmbg);
	
	
	
	List<Subscriber> findSubscriberByFirstNameContaining(String firstName);
	List<Subscriber> findSubscriberByLastNameContaining(String lastName);
	List<Subscriber> findSubscriberByJmbgContaining(String jmbg);
	
	@Query(value = getAllMerged, nativeQuery = true)
	List<Object[]> getAllMerged(Pageable pageable);
//	
	@Query(value = getAllMergedJmbg, nativeQuery = true)
	List<Object[]> getAllMergedJmbg(String jmbg, Pageable pageable);
//	
	@Query(value = getAllMergedFirstName, nativeQuery = true)
	List<Object[]> getAllMergedFirstName(String firstName, Pageable pageable);
//	
	@Query(value = getAllMergedLastName, nativeQuery = true)
	List<Object[]> getAllMergedLastName(String lastName, Pageable pageable);
//	
	@Query(value = getAllMergedFirstNameLastName, nativeQuery = true)
	List<Object[]> getAllMergedFirstNameLastName(String firstName, String lastName, Pageable pageable);
//	
	@Query(value = getAllMergedFirstNameJmbg, nativeQuery = true)
	List<Object[]> getAllMergedFirstNameJmbg(String firstName, String jmbg, Pageable pageable);
//	
	@Query(value = getAllMergedLastNameJmbg, nativeQuery = true)
	List<Object[]> getAllMergedLastNameJmbg(String lastName, String jmbg, Pageable pageable);
	
	@Query(value = getAllMergedFirstLastJmbg, nativeQuery = true)
	List<Object[]> getAllMergedFirstNameLastNameJmbg(String firstName, String lastName, String jmbg, Pageable pageable);
}
