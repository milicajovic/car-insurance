package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.car.Brand;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Long>{

}
