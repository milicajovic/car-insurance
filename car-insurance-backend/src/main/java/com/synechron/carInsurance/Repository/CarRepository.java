package com.synechron.carInsurance.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.car.Car;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

	@Query("SELECT c from Car c WHERE c.brand = ?1")
	List<Car> findAllCarsWithBrand(String brand);
	
	@Query("SELECT c from Car c WHERE c.brand = ?1 and c.model = ?2")
	List<Car> findAllCarsWithBrandAndModel(String brand,String model);
	
	@Query("SELECT c from Car c WHERE c.brand = ?1 and c.model = ?2 and c.year = ?3")
	Car findAllCarsWithBrandAndModelAndYear(String brand,String model, Integer year);
	
	@Query("SELECT c from Car c WHERE c.model = ?1")
	List<Car> findAllCarsWithModel(String model);
	
	@Query("SELECT c from Car c WHERE c.model = ?1 and c.year = ?2")
	List<Car> findAllCarsWithModelAndYear(String model, Integer year);
	
	@Query("SELECT c from Car c WHERE c.year = ?1")
	List<Car> findAllCarsWithYear(Integer year);
	
	@Query("SELECT c from Car c WHERE c.brand = ?1 and c.year =?2")
	List<Car> findAllCarsWithBrandAndYear(String brand, Integer year);
	
	@Query("SELECT c from Car c WHERE c.brand LIKE %?1% and c.year = ?2")
	List<Car> findByBrandAndYearContaining(String brand, Integer year, Pageable pageable);
	
	@Query("SELECT c from Car c WHERE c.model LIKE %?1% and c.year = ?2")
	List<Car> findByModelAndYearContaining(String model, Integer year, Pageable pageable);
	
	@Query("SELECT c from Car c WHERE c.brand LIKE %?1% and c.model LIKE %?2%")
	List<Car> findByBrandAndModelContaining(String brand, String model, Pageable pageable);
	
	List<Car> findByBrandContaining(String brand, Pageable pageable);
	List<Car> findByModelContaining(String model, Pageable pageable);
	List<Car> findByYear(Integer year, Pageable pageable);

	@Query("SELECT c from Car c WHERE c.brand LIKE %?1% and c.model LIKE %?2% and c.year = ?3")
	List<Car> findAllCarsWithBrandAndModelAndYearPageable(String brand,String model, Integer year, Pageable pageable);
	
	@Query("SELECT c from Car c")
	List<Car> findAllPageable(Pageable pageable);
}
