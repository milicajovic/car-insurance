package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.carInsurance.model.proposal.InsuranceItem;


public interface InsuranceItemRepository extends JpaRepository<InsuranceItem, Long>{

}
