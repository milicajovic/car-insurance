package com.synechron.carInsurance.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.car.Car;
import com.synechron.carInsurance.model.users.City;
import com.synechron.carInsurance.model.users.Country;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
	
	@Query("SELECT c from City c WHERE c.country.id = ?1")
	List<City> findAllCitiesOfCountry(Long countryId);
}
