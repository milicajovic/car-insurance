package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.carInsurance.model.users.MaritalStatus;

public interface MaritalStatusRepository extends JpaRepository<MaritalStatus, Long> {

}
