package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.carInsurance.model.car.Car;
import com.synechron.carInsurance.model.users.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
	
}
