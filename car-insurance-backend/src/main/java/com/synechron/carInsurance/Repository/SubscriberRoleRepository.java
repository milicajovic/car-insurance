package com.synechron.carInsurance.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.synechron.carInsurance.model.users.SubscriberRole;

public interface SubscriberRoleRepository extends JpaRepository<SubscriberRole, Long> {

}
