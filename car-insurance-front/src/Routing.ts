import { Routes } from "@angular/router";
import { AddPersonComponentComponent } from "./app/add-person-component/add-person-component.component";


export const routes: Routes =
    [
        {path: 'addPerson', component:AddPersonComponentComponent}

    ]
