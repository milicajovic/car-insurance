import { Routes } from "@angular/router";
import { AddPersonComponentComponent } from "./add-person-component/add-person-component.component";
import { AllsubComponent } from "./allsub/allsub.component";
import { SelectCarComponent } from "./select-car/select-car.component";
import { SearchDriverComponent } from "./search-driver/search-driver.component";
import { AddDriverComponent } from "./add-driver/add-driver.component";
import { CarCrudComponent } from "./crud/car-crud/car-crud.component";
import { AddCarComponent } from "./crud/car-crud/add-car/add-car.component";

import { InsurancePlanComponent } from "./insurance-plan/insurance-plan.component";

import { PersonCrudComponent } from "./crud/person-crud/person-crud.component";
import { ReviewProposalComponent } from "./review-proposal/review-proposal.component";
import { LoginComponent } from "./login/login.component";
import { ActivateGuard } from "./activate-guard";

export const routes: Routes =
    [
       
        {path: '', redirectTo: 'login', pathMatch: 'full'},
        {path: 'login', component:LoginComponent},
        {path: 'addPerson', component:AddPersonComponentComponent},
        {path: 'newProposal/:id', component:SelectCarComponent},
        {path: 'searchCar', component:SelectCarComponent},
        {path: 'withBrand:/brand', component:SelectCarComponent},
        {path: 'withBrandAndModel:/brand/:model', component:SelectCarComponent},
        {path: 'search/:jmbg', component: AllsubComponent},
        {path: 'allSubscribers', component:AllsubComponent},
        {path: 'searchDriver', component:SearchDriverComponent},
        {path: 'addDriver', component:AddDriverComponent},
        {path: 'carCrud', component:CarCrudComponent, canActivate: [ActivateGuard]},
        {path: 'newCar', component:AddCarComponent, canActivate: [ActivateGuard]},
        {path: 'insurancePlans', component:InsurancePlanComponent},
        {path: 'personCrud', component:PersonCrudComponent, canActivate: [ActivateGuard]},
        {path: 'getProposalById/:proposalID', component:ReviewProposalComponent}	


    ]
