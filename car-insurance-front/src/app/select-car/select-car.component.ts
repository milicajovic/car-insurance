import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CarService } from '../services/car.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SettingLicencePlateDTO } from '../model/settingLicencePlateNumber';

@Component({
  selector: 'app-select-car',
  templateUrl: './select-car.component.html',
  styleUrls: ['./select-car.component.css']
})
export class SelectCarComponent implements OnInit {

  brandsDropdownSettings: any;

  cars: any = [];
  models: any = [];
  years: any = [];
  brand: any;
  form: any = FormGroup;
  selectedBrandCode: any = String;
  brands: string[] = new Array;
  selectedBrand: any = String;
  selectedModel: any = String;
  selectedYear: any = Number;
  liceneceNum: any ;
  setingLicencePlateNumber : any;
  constructor(private carService: CarService, private router: Router, private formBuilder: FormBuilder) {
  }


  ngOnInit(): void {
    this.carService.brands().subscribe((data) => {
      console.log(data);
      this.brands = data;
      console.log(this.brands);
    })
  }

  

  getModels(brand: any) {
    alert(brand)
    this.carService.getModels(brand).subscribe((data) => {
      this.models = data;
      console.log(this.models);
    },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }



  getYears(brand: any, model: any) {
    alert(model)
    this.carService.getYears(brand, model).subscribe(
      data => {
        this.years = data;
        console.log(this.years);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  setLicencePlateNumber() {
    console.log(this.selectedModel+ " " +this.selectedBrand,this.selectedYear+ " " +
      this.liceneceNum+ " " +localStorage.getItem("proposalID"));
      this.setingLicencePlateNumber = new SettingLicencePlateDTO(this.selectedBrand,
        this.selectedModel,this.selectedYear,
        this.liceneceNum,localStorage.getItem("proposalID"));
    this.carService.setLicencePlateNumber(this.setingLicencePlateNumber).subscribe(
      (data) => {

      }

    );
  }


}
