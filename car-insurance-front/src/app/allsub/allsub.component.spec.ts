import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllsubComponent } from './allsub.component';

describe('AllsubComponent', () => {
  let component: AllsubComponent;
  let fixture: ComponentFixture<AllsubComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllsubComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllsubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
