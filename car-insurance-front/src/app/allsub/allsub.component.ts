import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CarService } from '../services/car.service';

@Component({
  selector: 'app-allsub',
  templateUrl: './allsub.component.html',
  styleUrls: ['./allsub.component.css']
})
export class AllsubComponent implements OnInit {



  subs: any = [];
  id: any;
  searchMode: boolean = false;
  jmbg: any;

  constructor(private service: CarService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.list();
    })
  }


  searchSub() {
    const jmbg = this.route.snapshot.paramMap.get("jmbg");
    this.service.searchSubscriber(jmbg).subscribe(data => {
      this.subs = []
      this.subs.push(data);
      console.log(data);
    });
  }

  list() {
    this.searchMode = this.route.snapshot.paramMap.has('jmbg');
    if (this.searchMode) {
      this.searchSub();
    } else {
      this.getAll();

    }
  }


  getAll() {
    this.service.getAll().subscribe(
      data => {
        this.subs = data;
      }
    )
  }

  newProposal(id:any) {
    console.log(id)
    this.service.newProposal(id).subscribe((response) => {
    },
      error => {
        console.log(error);
      }
    );
  }


}
