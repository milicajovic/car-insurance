import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
  })
export class ActivateGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){

        if (localStorage.getItem('user')) {
            alert('You are not allowed to view this page');
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }

}





