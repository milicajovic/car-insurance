import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Address } from 'src/app/model/address';
import { City } from 'src/app/model/city';
import { Country } from 'src/app/model/country';
import { Gender } from 'src/app/model/gender';
import { MaritalStatus } from 'src/app/model/maritalStatus';
import { PersonDTO } from 'src/app/model/personDTO';
import { AddressService } from 'src/app/services/address.service';
import { CityService } from 'src/app/services/city.service';
import { CountryService } from 'src/app/services/country.service';
import { GenderService } from 'src/app/services/gender.service';
import { MaritalStatusService } from 'src/app/services/marital-status.service';
import { PersonService } from 'src/app/services/person.service';
import { SubscriberService } from 'src/app/services/subscriber.service';
import { ModalService } from 'src/app/_modal';

@Component({
  selector: 'app-person-crud',
  templateUrl: './person-crud.component.html',
  styleUrls: ['./person-crud.component.css']
})
export class PersonCrudComponent implements OnInit {
  persons : any = [];
  genders : any = [];
  maritalStatuses : any = [];
  firstName : String = "";
  lastName : String = "";
  jmbg : String = "";
  deleteMsg : String = "";
  editMsg : String = "";
  currentPerson : any;
  firstNamePlaceholder : String = "";
  lastNamePlaceholder : String = "";
  maritalStatusPlaceholder : String = "";
  genderPlaceholder : String = "";
  jmbgPlaceholder : String = "";
  birthPlaceholder : String = "";
  emailPlaceholder : String = "";
  homePhonePlaceholder : String = "";
  mobilePhonePlaceholder : String = "";
  addressPlaceholder : String = "";
  cityPlaceholder : String = "";
  streetNamePlaceholder : String = "";
  streetNumberPlaceholder : String = "";
  page : number = 0;
  pageSize : number = 2;

constructor(private personService: PersonService,private addressService: AddressService, private modalService : ModalService,
   private maritalStatusService : MaritalStatusService, private genderService : GenderService){
  
  }

  ngOnInit(): void {
    this.persons = this.personService.getAll(this.page, this.pageSize).subscribe(data => {
      this.persons = data
    });

    this.genders = this.genderService.getAll().subscribe(data => {
      this.genders = data;
    })

    this.maritalStatuses = this.maritalStatusService.getAll().subscribe(data => {
      this.maritalStatuses = data;
    })
  }

  onEdit(person : any){
    this.firstNamePlaceholder = person.firstName;
    this.lastNamePlaceholder = person.lastName;
    this.maritalStatusPlaceholder = person.maritalStatus + "";
    this.genderPlaceholder = person.gender + "";
    this.jmbgPlaceholder = person.jmbg;
    this.birthPlaceholder = person.birth;
    this.emailPlaceholder = person.email;
    this.homePhonePlaceholder = person.homePhone;
    this.mobilePhonePlaceholder = person.mobilePhone;
    this.addressPlaceholder = person.address + "";
    this.cityPlaceholder = person.city;
    this.streetNamePlaceholder = person.streetName;
    this.streetNumberPlaceholder = person.streetNumber;

    this.modalService.open("personEditModal");
  }
  
  onDelete(person : PersonDTO){
    const jmbg = person.jmbg;

    if(confirm("Are you sure you want to delete selected Car?")) {
      this.personService.deletePerson(jmbg).subscribe(data => {
        console.log(data);
      })
    }

    this.deleteMsg = "Person deleted successfully";
    location.reload();
  }

  onSave(form: NgForm){
    const value = form.value;
    const maritalStatus = Number(value.maritalStatus);
    const gender = Number(value.gender);

    const person = new PersonDTO(value.firstName, value.lastName, maritalStatus, gender, value.jmbg,
       value.birth, value.email, value.homePhone, value.mobilePhone, value.city, value.streetName, value.streetNumber);
       
    this.personService.editPerson(person).subscribe(data => {
      console.log(data);
    });  

    this.editMsg = "Person eddited successfully";
    form.reset();
  }

  searchPeople(event : any){
    if(this.firstName.length >= 3 || this.lastName.length >= 3 || this.jmbg.length >= 5){
      this.persons = this.personService.searchPeople(this.firstName, this.lastName, this.jmbg, this.page, this.pageSize).subscribe(data => {
        this.persons = data;
      })
    }

    else if(this.firstName === "" && this.lastName === "" && this.jmbg === ""){
      this.personService.getAll(this.page, this.pageSize).subscribe(
        data => {
          this.persons = data;
        }
      )
    }
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
   this.modalService.close(id);
  }

  onIncrement(){
    this.page++;
    this.searchPeople(null);
    console.log(this.page);
  }

  onDecrement(){
    this.page--;
    this.searchPeople(null);
    console.log(this.page);
  }
}
