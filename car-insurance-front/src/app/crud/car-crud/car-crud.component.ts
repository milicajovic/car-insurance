import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Form, NgForm } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { carDTO } from 'src/app/model/carDTO';
import { CarService } from 'src/app/services/car.service';
import { ModalService } from 'src/app/_modal';

@Component({
    selector: 'carCrud',
    templateUrl: './car-crud.component.html',
    styleUrls: ['./car-crud.component.css']
})
export class CarCrudComponent implements OnInit{
    cars : any = [];
    carsSearch : any = [];
    currentCar : carDTO;
    msg : String = "";
    deleteMSG : String = "";
    brand : string = "";
    model : string = "";
    year : string = "";
    brandPlaceholder : String = "";
    modelPlaceholder : String = "";
    yearPlaceholder : String = "";
    page : number = 0;
    constructor(private service: CarService, private router: Router, private modalService : ModalService){
      this.currentCar = new carDTO("k", "d", 2);
    }

    ngOnInit(): void {
        this.cars =  this.service.getAllCarsPageable(0, 5).subscribe(
            data => {
              this.cars = data;
            }
         )
    }

    searchCars(event : any){
      if(this.brand.length >= 3 || this.model.length >= 3 || this.year.length == 4){
        this.cars = this.service.searchCars(this.brand,this.model,this.year, this.page).subscribe(
          data => {
            this.cars = data;
          }
        );
        console.log(this.cars);
      }
      else if(this.brand === "" && this.model === "" && this.year === ""){
        this.service.getAllCars().subscribe(
          data => {
            this.cars = data;
          }
        )
      } 
    }
    onEdit(car : carDTO){
      this.currentCar = car;
      this.brandPlaceholder = this.currentCar.brand;
      this.modelPlaceholder = this.currentCar.model;
      this.yearPlaceholder = "" + this.currentCar.year;
      this.modalService.open("carEditModal");
    }

    getAll() {
        this.service.getAllCarsPageable(0,5).subscribe(
          data => {
            this.cars = data;
          }
        )
      }

    addCar(){
      this.router.navigateByUrl("/newCar");
    }

    onDelete(car : carDTO){
      if(confirm("Are you sure you want to delete selected Car?")) {
        this.service.deleteCar(car).subscribe(responseData => {
          console.log(responseData);
        });
        this.deleteMSG = "Car deleted successfully";
        location.reload();
      }
    }

    onSave(form : NgForm){
     const value = form.value;
     const year = Number(value.year);
     const car = new carDTO(value.brand, value.model, year);
     car.id = this.currentCar.id;
     this.service.editCar(car).subscribe(responseData => {
       console.log(responseData);
      });
      form.reset();
      this.msg = "Car edited successfully";
    }

    openModal(id: string) {
      this.modalService.open(id);
    }

    closeModal(id: string) {
     this.modalService.close(id);
    }

    onIncrement(){
      this.page++;
      this.searchCars(null);
    }

    onDecrement(){
      this.page--;
      this.searchCars(null);
    }
}
