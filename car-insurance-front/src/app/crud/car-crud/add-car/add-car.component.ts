import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { carDTO } from 'src/app/model/carDTO';
import { CarService } from 'src/app/services/car.service';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})
export class AddCarComponent implements OnInit {
  msg : String = "";

  constructor(private carService : CarService, private router: Router) { }

  ngOnInit(): void {
  }

  onAddItem(form : NgForm){
    const value = form.value;
    const year = Number(value.year);
    const car = new carDTO(value.brand, value.model, year);
    console.log(car);
   this.carService.addCar(car).subscribe(responseData => {
     console.log(responseData);
   });
    form.reset();
    this.msg = "Car added successfully";
  }

  onGoToCrud(){
    this.router.navigateByUrl("/carCrud");
  }
}
