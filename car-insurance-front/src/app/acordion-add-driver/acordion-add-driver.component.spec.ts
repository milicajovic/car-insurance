import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcordionAddDriverComponent } from './acordion-add-driver.component';

describe('AcordionAddDriverComponent', () => {
  let component: AcordionAddDriverComponent;
  let fixture: ComponentFixture<AcordionAddDriverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcordionAddDriverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcordionAddDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
