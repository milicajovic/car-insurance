import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Address } from '../model/address';
import { City } from '../model/city';
import { Country } from '../model/country';
import { Driver } from '../model/driver';
import { Gender } from '../model/gender';
import { MaritalStatus } from '../model/maritalStatus';
import { AddressService } from '../services/address.service';
import { CarService } from '../services/car.service';
import { CityService } from '../services/city.service';
import { CountryService } from '../services/country.service';
import { DriverService } from '../services/driver.service';
import { SubscriberService } from '../services/subscriber.service';

@Component({
  selector: 'app-acordion-add-driver',
  templateUrl: './acordion-add-driver.component.html',
  styleUrls: ['./acordion-add-driver.component.css']
})
export class AcordionAddDriverComponent implements OnInit {

  closeResult = '';
  ris: any = [];
  public accidentHistory: any = {};
  public countries: Country[];
  public cities: City[];
  public addresses: Address[];
  genders: Gender[];
  selectedGender : string;
  maritalStatuses: MaritalStatus[];
  selectedMaritalStatus: string;
  public driver: any = {};
  public jmbg: number = 0;
  @Output() addingDriver = new EventEmitter(); 
  @Output() addingExistingDriver = new EventEmitter(); 
  
 constructor(private subscriberService: SubscriberService,private driverService : DriverService, private countryService: CountryService,
  private cityService: CityService,
  private addressService: AddressService,
  private modalService: NgbModal,
  private service: CarService
  
) {
  this.countries = new Array,
    this.cities = new Array,
    this.addresses = new Array,
    this.maritalStatuses = new Array,
    this.genders = new Array,
    this.selectedMaritalStatus = "",
    this.selectedGender = ""
    
}

  ngOnInit(): void {
    this.countryService.countries().subscribe((data) => {
      console.log(data);
      this.countries = data;
      console.log(this.countries);
    })
    
    this.subscriberService.maritalStatuses().subscribe(data => {
      this.maritalStatuses = data
      console.log(this.maritalStatuses)

    });
    this.subscriberService.genders().subscribe(data => {
      this.genders = data
      console.log(this.genders)

    });

    this.risks();
  }

  confirmCountry() {
    this.cityService.cities(this.driver.country.id).subscribe((data) => {
      console.log(data);
      this.cities = data;
      console.log(this.cities);
    })
  }
  confirmCity() {
    this.addressService.addresses(this.driver.city.id).subscribe((data) => {
      console.log(data);
      this.addresses = data;
      console.log(this.cities);
    })
  }
  addNewDriver(driver: any) {
    this.addingDriver.emit(driver);
    
  }
  addExistingDriver() {
    this.addingExistingDriver.emit(this.jmbg);
  }

  
   open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openA(contentA) {
    this.modalService.open(contentA, {ariaLabelledBy: 'modal-basic-title1'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  risks(){
    this.service.getRisks().subscribe(
      data =>{
        this.ris=data;
      }
    )
  }

  addAccident(accidentHistory:any,id:any){
    this.driverService.newAccident(accidentHistory,id).subscribe(
      data=>{

      }
    )
  }


}
