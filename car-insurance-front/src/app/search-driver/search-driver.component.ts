import { Component, OnInit } from '@angular/core';
import { CarService } from '../services/car.service';

@Component({
  selector: 'app-search-driver',
  templateUrl: './search-driver.component.html',
  styleUrls: ['./search-driver.component.css']
})
export class SearchDriverComponent implements OnInit {

  drivers: any = [];

  constructor(private service: CarService) { }

  ngOnInit(): void {
    this.getAllDrivers();
    
  }

  searchDriver(jmbg:any) {
    this.service.searchDrvr(jmbg).subscribe(data => {
      this.drivers = []
      this.drivers.push(data);
      console.log(data);
    });
  }

  getAllDrivers(){
    this.service.getDrivers().subscribe(data=>{
      this.drivers=data;
    })
  }

}


