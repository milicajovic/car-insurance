import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../model/user';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new User();

  constructor(private service: LoginService, private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    this.service.loginUser(this.user).subscribe(
      data => {
        
        if(this.user.username=="agent"){
          localStorage.setItem('user', JSON.stringify(data));
          localStorage.setItem("idUser", data.id);
          this.router.navigate(['allSubscribers']);
        }else{
          this.router.navigate(['carCrud']);
        }
      }
    )
  }

  logout(){
    localStorage.removeItem("user");
    this.router.navigate(['login']);
  }

}
