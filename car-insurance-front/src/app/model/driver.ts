
export class Driver {
    firstName : string;
    lastName : string;
    maritalStatus : number;
    gender : number;
    jmbg : string;
    birth : string;
    email : string;
    homePhone : string;
    mobilePhone : string;
    address : number;
    licenceNum : string;
    licenceObtained : string;
    yearsInsured : number;
   
    constructor(firstName:string, lastName:string,maritialStatus:number,gender:number,jmbg : string,
        datebirth : string,email : string,homePhone : string, mobilePhone : string,
        address : number, licenceNum : string,licenceObtained : string, yearsInsured : number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.maritalStatus = maritialStatus;
        this.gender = gender;
        this.jmbg = jmbg;
        this.birth = datebirth;
        this.email = email;
        this.homePhone = homePhone;
        this.mobilePhone = mobilePhone;
        this.address = address;
        this.licenceNum = licenceNum;
        this.licenceObtained = licenceObtained;
        this.yearsInsured = yearsInsured;
    }
    

} 