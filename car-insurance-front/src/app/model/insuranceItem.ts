import { Franchise } from "./Franchise";

export class InsuranceItem {
    id : number;
    name : string;
    isOptional : boolean;
    franchiseO : Franchise;
    franchise : number
    amount : number;
    constructor(id:number, name:string,isOptional : boolean, franchiseO :Franchise, franchise : number, amount : number ) {
        this.id = id;
        this.name = name;
        this.isOptional = isOptional;
        this.franchiseO = franchiseO;
        this.franchise = franchise
        this.amount = amount;
    } 
} 