import { InsurancePlan } from "./insurancePlan";

export class SettingPlanOnProposal {
    public plan : InsurancePlan;
    public idProposal : any;
    
    constructor(plan:InsurancePlan, idProposal:any ) {
        this.plan = plan;
        this.idProposal = idProposal;
     
    } 
    getPlan() : InsurancePlan {
        return this.plan;
    }
    getidProposal() : any {
        return this.idProposal;
    }
} 