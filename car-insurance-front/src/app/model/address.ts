import { City } from "./city";

export class Address {
    id : number;
    street : string;
    streetNumber : string;
    city : City;
    isDeleted : boolean = false;
    constructor(id:number, street:string,streetNumber : string, city :City ) {
        this.id = id;
        this.street = street;
        this.streetNumber = streetNumber;
        this.city = city;
    } 
} 