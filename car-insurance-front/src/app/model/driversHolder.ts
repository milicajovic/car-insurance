import { Driver } from "./driver";

export class DriversHolder {
    drivers : Driver[];
   
    constructor(drivers: Driver[]) {
        this.drivers = drivers;
    }

} 