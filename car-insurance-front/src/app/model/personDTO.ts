import { Gender } from "./gender";
import { MaritalStatus } from "./maritalStatus";

export class PersonDTO{
    constructor(public firstName : String, public lastName : String, public maritalStatus : number, public gender : number,
        public jmbg : String, public birth : String, public email : String, public homePhone : String,
         public mobilePhone : String, public city : String, public streetName : String, public streetNumber : String){
            
         }

        
}