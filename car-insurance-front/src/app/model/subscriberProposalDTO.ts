export class SubscriberProposalDTO {
    public idP : number;
    public idS : number;
    
    constructor(idP:number, idS:number ) {
        this.idP = idP;
        this.idS = idS;
     
    } 
    getidP() : number {
        return this.idP;
    }
    getIdS() : number {
        return this.idS;
    }
} 