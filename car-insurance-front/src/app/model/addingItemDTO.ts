export class AddingItemDTO {
    private planId : number;
    private itemId : number;
    
    constructor(itemId:number, planId:number ) {
        this.planId = planId;
        this.itemId = itemId;
     
    } 
    getPlanId() : number {
        return this.planId;
    }
    getItemId() : number {
        return this.itemId;
    }
} 