import {InsuranceItem} from "./insuranceItem";
export class InsurancePlan {
    id : number;
    name : string;
    isPremium : boolean;
    amount : number;
    insuranceItems : InsuranceItem[] = new Array;
    constructor(id:number, name:string,isPremium : boolean, amount :number, insuranceItems : InsuranceItem[] ) {
        this.id = id;
        this.name = name;
        this.isPremium = isPremium;
        this.amount = amount;
        this.insuranceItems = insuranceItems;
    } 
} 