export class SettingLicencePlateDTO {
    brand : string;
    model : string;
    year : number;
    licencePlateNumber : string;
    proposalID : any;
    constructor(brand:string, model:string, year : number,
        licencePlateNumber : string, proposalId : any) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.licencePlateNumber = licencePlateNumber;
        this.proposalID = proposalId;
    }
} 