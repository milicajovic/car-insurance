export class Country {
    id : number;
    name : string;
    isDeleted : boolean = false;
    constructor(id:number, name:string) {
        this.id = id;
        this.name = name;
    }
} 