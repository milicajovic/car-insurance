export class carDTO{
id;
    constructor(public brand : String, public model : String, public year : number){
        this.brand = brand;
        this.model = model;
        this.year = year;
    }

    public getModel() : String{
        return this.model;
    }

    public getBrand() : String{
        return this.brand;
    }

    public getYear() : String{
        return "" + this.year;
    }
}