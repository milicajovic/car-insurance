import { Country } from "./country";

export class City {
    id : number;
    name : string;
    zipcode : number;
    Country : Country;
    isDeleted : boolean = false;
    constructor(id:number, name:string,zipcode : number, country :Country ) {
        this.id = id;
        this.name = name;
        this.zipcode = zipcode;
        this.Country = country;
    } 
} 