import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InsurancePlanService } from '../services/insurance-plan.service';
import { ProposalService } from '../services/proposal.service';

@Component({
  selector: 'app-review-proposal',
  templateUrl: './review-proposal.component.html',
  styleUrls: ['./review-proposal.component.css']
})
export class ReviewProposalComponent implements OnInit {

  prop: any = [];
  drivers: any= [];
  items: any = [];
  idPlan:any;
  id = localStorage.getItem("idPlan");
  constructor(private service:ProposalService, private route: ActivatedRoute, private insuranceService:InsurancePlanService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      this.reviewProposal();
      this.driversOnProposal();
      this.itemsInsPlan();
    })
    
  }

  reviewProposal(){
    const proposalID = this.route.snapshot.paramMap.get('proposalID');
    this.service.getProposal(proposalID).subscribe(
      data=>{
        this.prop = []
        this.prop.push(data);
        console.log(data);
        localStorage.setItem("idPlan", data.insurancePlan.id);
        console.log(data.insurancePlan.id);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }


  driversOnProposal(){
    const proposalID = this.route.snapshot.paramMap.get('proposalID');
    this.service.getDriversProposal(proposalID).subscribe(
      data =>{
        this.drivers=data;
       
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  itemsInsPlan(){
    this.insuranceService.itemsOfInsurancePlan(this.id).subscribe(
      data => {
        this.items=data;
        console.log(data);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  exportToPdf(){
    const proposalID = this.route.snapshot.paramMap.get('proposalID');
    this.service.report(proposalID).subscribe(response =>{

    }
    )
  }

  

}
