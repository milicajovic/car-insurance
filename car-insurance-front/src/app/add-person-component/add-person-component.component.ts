import { Component, OnInit } from '@angular/core';
import { CarService } from '../services/car.service';
import { Address } from '../model/address';
import { City } from '../model/city';
import { Country } from '../model/country';
import { AddressService } from '../services/address.service';
import { CityService } from '../services/city.service';
import { CountryService } from '../services/country.service';
import { SubscriberService } from '../services/subscriber.service';
import { MaritalStatus } from '../model/maritalStatus';
import { Gender } from '../model/gender';

@Component({
  selector: 'app-add-person-component',
  templateUrl: './add-person-component.component.html',
  styleUrls: ['./add-person-component.component.css']
})
export class AddPersonComponentComponent implements OnInit {

  public sub: any = {};
  public countries: Country[];
  public cities: City[];
  public addresses: Address[];
  genders: Gender[];
  selectedGender : string;
  maritalStatuses: MaritalStatus[];
  selectedMaritalStatus: string;
  radioSelected:Gender;

  constructor(private subscriberService: SubscriberService, private countryService: CountryService,
    private cityService: CityService,
    private addressService: AddressService,
  ) {
    this.countries = new Array,
      this.cities = new Array,
      this.addresses = new Array,
      this.maritalStatuses = new Array,
      this.genders = new Array,
      this.selectedMaritalStatus = "",
      this.selectedGender = "",
      this.radioSelected = new Gender(1,"female")
  }

  ngOnInit(): void {
    this.countryService.countries().subscribe((data) => {
      console.log(data);
      this.countries = data;
      console.log(this.countries);
    })
    
    this.subscriberService.maritalStatuses().subscribe(data => {
      this.maritalStatuses = data
      console.log(this.maritalStatuses)

    });
    this.subscriberService.genders().subscribe(data => {
      this.genders = data
      console.log(this.genders)

    });
  }

  getCities(countryId: number) {
    this.cityService.cities(countryId).subscribe((data) => {
      console.log(data);
      this.cities = data;
      console.log(this.cities);
    })
  }

  getAddresses(cityId: number) {
    this.addressService.addresses(cityId).subscribe((data) => {
      console.log(data);
      this.addresses = data;
    })
  }

  addSubscriber(sub: any) {
    console.log(sub)
    alert(this.selectedMaritalStatus)
    this.subscriberService.addSubscriber(sub).subscribe(
      data => {
        localStorage.setItem("proposalID",data.idP);
        localStorage.setItem("subscriberId",data.idS);
        console.log( localStorage.getItem("subscriberId"));
        console.log( localStorage.getItem("proposalID"));
        console.log("response received");
      }
    )
  }
  confirmCountry() {
    this.cityService.cities(this.sub.country.id).subscribe((data) => {
      console.log(data);
      this.cities = data;
      console.log(this.cities);
    })
  }
  confirmCity() {
    this.addressService.addresses(this.sub.city.id).subscribe((data) => {
      console.log(data);
      this.addresses = data;
      console.log(this.cities);
    })
  }


  radioFun(){
    alert(this.radioSelected.name)
  }
}
