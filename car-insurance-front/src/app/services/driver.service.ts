import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Driver } from '../model/driver';
import { DriversHolder } from '../model/driversHolder';

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  private baseUrl = "http://localhost:8080";
  private driver!: Driver;
  private driversHolder!: DriversHolder;
  constructor(private http: HttpClient) { }

  addDriver(driver:any): Observable<any>{
    console.log( driver.birth);
    this.driver = new Driver(driver.firstName,driver.lastName,
      driver.selectedMaritalStatus.id,driver.gender,driver.jmbg,
      driver.birth,
      driver.email,driver.homePhone,driver.mobilePhone,driver.address,
      driver.licenceNum,driver.licenceObtained,driver.yearsInsured);
      console.log( driver);
   return this.http.post(`${this.baseUrl}/api/v1/driver/addDriver`,this.driver);
  }
  addDrivers(drivers: Driver[]): Observable<any> {
    this.driversHolder  = new DriversHolder(drivers);
    drivers.forEach(element => {
      console.log(element.homePhone, element.mobilePhone+" servis"+ element.firstName)
    })
    return this.http.post(`${this.baseUrl}/api/v1/driver/addDriversOnProposal/`+localStorage.getItem("proposalID"),this.driversHolder);
  }

  newAccident(accidentHistory:any,id:any): Observable<any>{
    return this.http.post(`${this.baseUrl}/api/v1/driver/accidentHistory/${id}`,accidentHistory);
  }
  
}
