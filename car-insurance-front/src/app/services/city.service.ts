import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from '../model/city';

@Injectable({
  providedIn: 'root'
})
export class CityService {
  private baseUrl = "http://localhost:8080";

  constructor(private http: HttpClient) { }

  cities(countryId : number): Observable<City[]>{
    
    return this.http.get<City[]>(`${this.baseUrl}/api/v1/city/`+countryId);
  }
}
