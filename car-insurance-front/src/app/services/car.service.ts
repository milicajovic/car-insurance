import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SettingLicencePlateDTO } from '../model/settingLicencePlateNumber';
import { carDTO } from '../model/carDTO';

@Injectable({
  providedIn: 'root'
})
export class CarService {
  private baseUrl = "http://localhost:8080";

  constructor(private http: HttpClient) { }


  getAll():Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/subscriber/allSubscribers`);
  }

  searchSubscriber(jmbg:any): Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/subscriber/search/${jmbg}`);
  }

  newProposal(id:any): Observable<any>{
    const headers= new HttpHeaders({"Access-Control-Allow-Origin": "*"});
    return this.http.post(`${this.baseUrl}/api/v1/proposal/newProposal/${id}`,{headers: headers});
  }

  addSub(subscriber:any): Observable<any>{
    return this.http.post(`${this.baseUrl}/api/v1/subscriber`,subscriber);
  }

  addCar(car:any) : Observable<any>{
    return this.http.post(`${this.baseUrl}/api/v1/car`, car);
  }
  
  editCar(car:any) : Observable<any>{
    return this.http.put(`${this.baseUrl}/api/v1/car/edit/${car.id}`,car);
  }

  deleteCar(car:any) : Observable<any>{
    return this.http.delete(`${this.baseUrl}/api/v1/car/${car.id}`);
  }
  
  getAllCars() : Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/car/allCars`);
  }

  getAllCarsPageable(page : number, pageSize : number) : Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/car/allCars/${page}/${pageSize}`);
  }
  
  brands(): Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/car/allBrands`);
  }

  getModels(brand:any):Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/car/withBrand/${brand}`);
  }
  
  getCar(brand:string, model : string, year : number):Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/car/withBrandModelAndYear/${brand}/${model}/${year}`);
  }

  getYears(brand:any,model:any):Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/car/withBrandAndModel/${brand}/${model}`);
  }
  setLicencePlateNumber(setingLicencePlate : SettingLicencePlateDTO) {
    console.log(setingLicencePlate);
    return this.http.post<any>(`${this.baseUrl}/api/v1/proposal/SetLicencePlate`,setingLicencePlate);
  }

 searchDrvr(jmbg:any):Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/driver/searchDriver/${jmbg}`)
  }

  getDrivers():Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/driver/allDrivers`);
  }

  getRisks(): Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/driver/allRisks`);
  }

  searchCars(brand : string, model : string, year : string, n : number) : Observable<any>{
    return this.http.post<any>(`${this.baseUrl}/api/v1/car/searchCars/${n}/2`, {"brand" : brand, "model" : model, "year" : year});
  }

}