import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AddingItemDTO } from '../model/addingItemDTO';
import { InsurancePlan } from '../model/insurancePlan';
import {SettingPlanOnProposal } from '../model/settingPlanOnProposal';

@Injectable({
  providedIn: 'root'
})
export class InsurancePlanService {
  private baseUrl = "http://localhost:8080";
 
  addingItem : AddingItemDTO
  constructor(private http: HttpClient) { 
   
    this.addingItem = new AddingItemDTO(0,0)
  }

  insurancePlans(): Observable<any[]>{
    
    return this.http.get<any[]>(`${this.baseUrl}/api/v1/insurancePlan/allInsurancePlans`);
  }
  insuranceItems(): Observable<any[]>{
    
    return this.http.get<any[]>(`${this.baseUrl}/api/v1/insurancePlan/AllInsuranceItems`);
  }
  itemsOfInsurancePlan(idPlan : any): Observable<any[]>{
    
    return this.http.get<any[]>(`${this.baseUrl}/api/v1/insurancePlan/ItemsOfInsurancePlan/`+idPlan);
  }
  setPlanOnProposal(plan: InsurancePlan,idProposal: any): Observable<any> {
    let settingPlanOnProposal  = new SettingPlanOnProposal(plan,idProposal)
    return this.http.post<any[]>(`${this.baseUrl}/api/v1/insurancePlan/setPlanOnProposal/`,settingPlanOnProposal);
  }
  addItemOnInsurancePlan(idPlan: number,iditem: number) : Observable<any> {
    this.addingItem = new AddingItemDTO(iditem,idPlan)
    return this.http.post<any[]>(`${this.baseUrl}/api/v1/insurancePlan/addItem`,this.addingItem);
  }
}
