import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Gender } from '../model/gender';

@Injectable({
  providedIn: 'root'
})
export class GenderService {
  private baseUrl = "http://localhost:8080";
  private gender! : Gender;

constructor(private http: HttpClient) { }


getAll() : Observable<any>{
  return this.http.get(`${this.baseUrl}/api/v1/gender/`);
}

}
