import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonDTO } from '../model/personDTO';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private baseUrl = "http://localhost:8080";
  private personDTO! : PersonDTO
  constructor(private http: HttpClient) { }

  getAll(page : number, pageSize : number) : Observable<any>{
    return this.http.get(`${this.baseUrl}/api/v1/person/${page}/${pageSize}`);
  }

  editPerson(person : PersonDTO) : Observable<any>{
    return this.http.put(`${this.baseUrl}/api/v1/person/editPerson`, person);
  }

  deletePerson(jmbg : String) : Observable<any>{
    return this.http.delete(`${this.baseUrl}/api/v1/person/${jmbg}`);
  }

  searchPeople(firstName : String, lastName : String, jmbg : String, page : number, pageSize : number) : Observable<any>{
    return this.http.post(`${this.baseUrl}/api/v1/person/searchPeople/${page}/${pageSize}`, {"firstName" : firstName, "lastName" : lastName, "jmbg" : jmbg});
  }
}
