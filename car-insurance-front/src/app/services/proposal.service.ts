import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProposalService {
  private baseUrl = "http://localhost:8080";

  constructor(private http: HttpClient) { }

  getProposal(proposalID:any):Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/proposal/getProposalById/${proposalID}`);
  }

  getDriversProposal(id:any):Observable<any>{
    return this.http.get<any>(`${this.baseUrl}/api/v1/proposal/driversProposal/${id}`);
  }

  report(proposalID:any){
    return this.http.get(`${this.baseUrl}/api/v1/proposal/report/${proposalID}`);
  }
}
