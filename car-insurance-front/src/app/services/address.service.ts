import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Address } from '../model/address';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  private baseUrl = "http://localhost:8080";

  constructor(private http: HttpClient) { }

  addresses(cityId : number): Observable<Address[]>{
    
    return this.http.get<Address[]>(`${this.baseUrl}/api/v1/address/`+cityId);
  }

  getAddress(id : number) : Observable<any>{
    return this.http.get(`${this.baseUrl}/api/v1/address/${id}`); 
  }
}
