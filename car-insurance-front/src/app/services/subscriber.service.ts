import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { Gender } from '../model/gender';
import { MaritalStatus } from '../model/maritalStatus';
import { SubsrcriberDTO } from '../model/subscriberDTO';

@Injectable({
  providedIn: 'root'
})
export class SubscriberService {
  private baseUrl = "http://localhost:8080";
  private subscriberDTO!: SubsrcriberDTO;
  
  constructor(private http: HttpClient) { }

  addSubscriber(subscriber:any): Observable<any>{
    console.log( subscriber.birth);
    this.subscriberDTO = new SubsrcriberDTO(subscriber.firstName,subscriber.lastName,
      subscriber.selectedMaritalStatus.id,subscriber.gender.id,subscriber.jmbg,
      subscriber.birth,
      subscriber.email,subscriber.homePhone,subscriber.mobilePhone,subscriber.address.id);
      console.log( subscriber);
   return this.http.post(`${this.baseUrl}/api/v1/subscriber`,this.subscriberDTO);
  }
  getSubscriber(): Observable<any> {
    return this.http.get(`${this.baseUrl}/api/v1/subscriber/searchById/`+localStorage.getItem("subscriberId"));
  }
 
  
  maritalStatuses(): Observable<MaritalStatus[]>{
    return this.http.get<MaritalStatus[]>(`${this.baseUrl}/api/v1/maritalStatuses`);
  }
  genders() : Observable<Gender[]> {
    return this.http.get<Gender[]>(`${this.baseUrl}/api/v1/gender`);
  }
}
