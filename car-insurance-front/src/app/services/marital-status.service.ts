import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MaritalStatus } from '../model/maritalStatus';

@Injectable({
  providedIn: 'root'
})
export class MaritalStatusService {
  private baseUrl = "http://localhost:8080";
  private maritalStatus! : MaritalStatus;

constructor(private http: HttpClient) { }


getAll() : Observable<any>{
  return this.http.get(`${this.baseUrl}/api/v1/maritalStatuses/`);
}


}
