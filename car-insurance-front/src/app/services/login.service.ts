import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private baseUrl = "http://localhost:8080";

  constructor(private http: HttpClient) { }

  loginUser(user:any):Observable<any>{
    return this.http.post<any>(`${this.baseUrl}/api/v1/login/loginUser`,user);
  }
}
