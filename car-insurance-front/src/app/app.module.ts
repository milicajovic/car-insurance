import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import * as bootstrap from "bootstrap";
import * as $ from "jquery";

import { AppComponent } from './app.component';
import { AddPersonComponentComponent } from './add-person-component/add-person-component.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {routes} from './Routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectCarComponent } from './select-car/select-car.component';
import { CarService } from './services/car.service';
import { SearchComponent } from './search/search.component';
import { AllsubComponent } from './allsub/allsub.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddDriverComponent } from './add-driver/add-driver.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatExpansionModule} from "@angular/material/expansion";
import { AcordionAddDriverComponent } from './acordion-add-driver/acordion-add-driver.component';
import { SearchDriverComponent } from './search-driver/search-driver.component';
import { CarCrudComponent } from './crud/car-crud/car-crud.component';
import { AddCarComponent } from './crud/car-crud/add-car/add-car.component';

import { InsurancePlanComponent } from './insurance-plan/insurance-plan.component';

import { ModalModule } from './_modal';
import { PersonCrudComponent } from './crud/person-crud/person-crud.component';
import { ReviewProposalComponent } from './review-proposal/review-proposal.component';

//import { MatDatepickerModule } from '@angular/material/datepicker';
//import {MatInputModule} from '@angular/material/input';
//import {MatNativeDateModule} from '@angular/material/core';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPersonComponentComponent,
    SelectCarComponent,
    SearchComponent,
    AllsubComponent,
    AddDriverComponent,
     SearchDriverComponent,
    AcordionAddDriverComponent,
    AcordionAddDriverComponent,
    AddCarComponent,
    CarCrudComponent,


    InsurancePlanComponent,

    PersonCrudComponent,

    InsurancePlanComponent,
    PersonCrudComponent,
    ReviewProposalComponent,
    LoginComponent


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    NgbModule,
    MatExpansionModule,
    ModalModule
  ],
  providers: [CarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
