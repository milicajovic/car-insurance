import { Component, OnInit } from '@angular/core';
import { InsurancePlanService } from '../services/insurance-plan.service';
import { InsurancePlan } from '../model/insurancePlan';
import { InsuranceItem } from '../model/insuranceItem';
import { VirtualTimeScheduler } from 'rxjs';
import { Franchise } from '../model/Franchise';

@Component({
  selector: 'app-insurance-plan',
  templateUrl: './insurance-plan.component.html',
  styleUrls: ['./insurance-plan.component.css']
})
export class InsurancePlanComponent implements OnInit {
  private ok: boolean = false;
  private has: boolean = false;
  private isPremium: boolean = false;
  
  
  private plansFromBackEnd: any;
  private itemsFromBackEnd: any;
  public plans: InsurancePlan[] = new Array();
  public items: InsuranceItem[] = new Array();
  public itemsOfInsurancePlan = new Array;
  private allItemsFromBackend = new Array();
  private allItems = new Array();
  private itemsOfPlan = new Array();
  private itemsOutOfPlan = new Array();
  radioSelected: InsurancePlan;

  constructor(private insurancePlanService: InsurancePlanService) {
    this.radioSelected = new InsurancePlan(0, "", false, 0, new Array())
  }

  ngOnInit(): void {
    this.insurancePlanService.insuranceItems().subscribe(
      data => {
        this.allItemsFromBackend = data;
        this.allItemsFromBackend.forEach(element => {
          let item = new InsuranceItem(element.id, element.name, element.isOptional, new Franchise(0),0, element.amount)
          this.allItems.push(item)
        })
      }
    );

    this.insurancePlanService.insurancePlans().subscribe(
      data => {
        this.plansFromBackEnd = data;
        this.plansFromBackEnd.forEach(element => {
          this.items = new Array();
          this.itemsFromBackEnd = element.insuranceItems;
          this.itemsFromBackEnd.forEach(element => {
            let item = new InsuranceItem(element.id, element.name, element.isOptional,new Franchise(0),0, element.amount)
            console.log(item.franchise)
            this.items.push(item);
          });
          let plan = new InsurancePlan(element.id, element.name, element.isPremium, element.amount, this.items)
          this.plans.push(plan);
        });
      });
    console.log(this.plans.length)
    this.plans.forEach(element => {
      console.log(element.name + " ___ ")
    })
    this.insurancePlanService.insuranceItems().subscribe(
      data => {
        this.items = data;
      });
  }
  show(idItem: number, plan: InsurancePlan): boolean {
    this.ok = false;
    plan.insuranceItems.forEach(element => {
      if (element.id == idItem) {
        this.ok = true;
      }
    })
    return this.ok;
  }
  itemsInPlan(planId: number) {
    this.itemsOfPlan = new Array();
    this.plans.forEach(element => {
      if (element.id == planId) {
        this.itemsOfPlan = element.insuranceItems
      }
    })
    return this.itemsOfPlan;
  }
  choosePlan() {
    console.log(this.itemsOfPlan)
    console.log(this.itemsOutOfPlan)
    console.log(this.radioSelected.id + " ++++ " + this.radioSelected.name)
    let finalItems = this.itemsOfPlan

    this.itemsOutOfPlan.forEach(element => finalItems.push(element))
    console.log(finalItems)

    this.insurancePlanService.setPlanOnProposal(this.radioSelected, localStorage.getItem("proposalID")).subscribe(
      data => {
        console.log(data.id)
      });
    this.allItems.forEach(element => {
      console.log(element.franchise + " frasiza ")
    })
  }
  itemsNotInPlan(planId: number) {
    this.itemsOutOfPlan = new Array();
    this.allItems.forEach(element1 => {
      this.has = false;
      this.itemsOfPlan.forEach(element2 => {
        if (element1.id == element2.id) {
          this.has = true;
        }
      })
      if (this.has == false) {
        this.itemsOutOfPlan.push(element1)
      }
    })


    return this.itemsOutOfPlan
  }

  addItem(planId: number, itemId: number) {
    console.log("plan" + planId + "item " + itemId)
    this.plans.forEach(element1 => {
      if (element1.id == planId) {
        this.items.forEach(element2 => {
          if (element2.id == itemId)
            element1.insuranceItems.push(element2)
        })
      }
    })
    this.insurancePlanService.addItemOnInsurancePlan(planId, itemId).subscribe(
      data => {
      }
    )
  }
  added(plan: InsurancePlan, itemId: number) : boolean  {
    let ok = false;
    plan.insuranceItems.forEach(element => {
      if (element.id == itemId) {
        ok = true;
      }
    })
    return ok
  }


}
