import { Component, OnInit } from '@angular/core';
import { Address } from '../model/address';
import { City } from '../model/city';
import { Country } from '../model/country';
import { Gender } from '../model/gender';
import { MaritalStatus } from '../model/maritalStatus';
import { AddressService } from '../services/address.service';
import { CityService } from '../services/city.service';
import { CountryService } from '../services/country.service';
import { SubscriberService } from '../services/subscriber.service';
import { DriverService } from '../services/driver.service';
import { Driver } from '../model/driver';

@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.css']
})
export class AddDriverComponent implements OnInit {
  public numberOfDrivers!: number;
  public numbers : number[] = new Array();
  public licenceNumDriver : string = ""
  public licenceObtainedDriver : string = ""
  public yearsInsuredDriver : number = 0
  private drivers : Driver[] = new Array();
  private driverService : DriverService;
  private subscriberService : SubscriberService;
  private existingDriver: Driver | undefined;
  private subscriberDriver!: Driver;
  public sub: boolean  = false;


 constructor(driverService : DriverService, subscriberService: SubscriberService) {   
  this.driverService = driverService
  this.subscriberService = subscriberService;
 }
  ngOnInit(): void {
   
  }

  acordions() : void {
    console.log(this.numberOfDrivers);
    this.numbers = new Array();
    for (let i = 1; i <= this.numberOfDrivers; i++) {
      this.numbers.push(i);
      }
      console.log(this.numbers);
  }
  addDriver(driver) {
    this.driverService.addDriver(driver).subscribe(
      data => {

      }
    )
    if ( this.drivers.length == 4 ) {
      alert("can not add more drivers");
    } else {
    this.drivers.push(driver);
    console.log(this.drivers);
    }
  }
addDrivers() {
  console.log(this.drivers.length+ " " + localStorage.getItem("proposalID")+" "+this.drivers);
    this.driverService.addDrivers(this.drivers).subscribe(
      data => {
        console.log(this.drivers.length+ " " + localStorage.getItem("proposalID"));
        
      }
    )
  }
  addExistingDriver(jmbg: string) {
    this.existingDriver = new Driver("","",0,0,jmbg,"","","","",0,"","",0);
    if ( this.drivers.length == 4 ) {
      alert("can not add more drivers");
    } else {
      this.drivers.push(this.existingDriver);
      console.log(this.existingDriver);
    }

  }
  addSubscriberDriver() {
    console.log("usap")
    this.subscriberService.getSubscriber().subscribe(
      data => {
        
        this.subscriberDriver = new Driver(data.firstName,data.lastName,data.maritalStatus.id,data.gender.id,
          data.jmbg,data.birth,data.email,data.homePhone,data.mobilePhone,data.address.id
          ,this.licenceNumDriver,this.licenceObtainedDriver,this.yearsInsuredDriver);
         
          if ( this.drivers.length == 4 ) {
            alert("can not add more drivers");
          } else {
            console.log("else"+ this.subscriberDriver+ " else")
            this.drivers.push(this.subscriberDriver);
            console.log(this.existingDriver);
            console.log(this.drivers);
          }
      }
    )
   
  }
  addSubscriber() {
  this.sub = true;
  }
}
